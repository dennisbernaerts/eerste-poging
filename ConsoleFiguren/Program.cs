﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleFiguren
{
    class Program
    {
        static void Main(string[] args)
        {
            Program_Dennis.circle();

            Console.ReadKey();
        }
    }

    class Program_Vic
    {
        static void run(string[] args)
        {
            #region OefeningTabel
            Console.WriteLine("Hoe breed wens je de kolon?");
            int breedte = int.Parse(Console.ReadLine());


            string[] headers = { "categorie", "artikel", "merk", "prijs", "stock", "nu besteld geleverd op".PadRight(breedte) };
            string[] categorie = { "meubels", "electronica", "electronica".PadRight(breedte) };
            string[] artikel = { "stoel", "tv", "watch".PadRight(breedte) };
            string[] merk = { "ikea", "samsung", "gamin".PadRight(breedte) };
            string[] prijs = { "100", "500", "200" };
            string[] stock = { Convert.ToString(true), Convert.ToString(false), Convert.ToString(true) };
            string[] nuBesteld = { Convert.ToString(DateTime.Parse("Sunday 26 April")), Convert.ToString(DateTime.Parse("Sunday 26 April")), Convert.ToString(DateTime.Parse("Sunday 26 April")) };


            Console.WriteLine(headers[0].PadRight(breedte) + headers[1].PadRight(breedte) + headers[2].PadRight(breedte) + headers[3].PadRight(breedte) + headers[4].PadRight(breedte) + headers[5].PadRight(breedte));
            Console.WriteLine(categorie[0].PadRight(breedte) + artikel[0].PadRight(breedte) + merk[0].PadRight(breedte) + prijs[0].PadRight(breedte) + stock[0].PadRight(breedte) + nuBesteld[0].PadRight(breedte));
            Console.WriteLine(categorie[1].PadRight(breedte) + artikel[1].PadRight(breedte) + merk[1].PadRight(breedte) + prijs[1].PadRight(breedte) + stock[1].PadRight(breedte) + nuBesteld[1].PadRight(breedte));
            Console.WriteLine(categorie[2].PadRight(breedte) + artikel[2].PadRight(breedte) + merk[2].PadRight(breedte) + prijs[2].PadRight(breedte) + stock[2].PadRight(breedte) + nuBesteld[2].PadRight(breedte));


            Console.WriteLine("Druk op u om de kolonnen breder te maken, of s om te stoppen.");
            char brederMaker = 'a';
            brederMaker = Convert.ToChar(Console.Read());
            while (brederMaker != 's')
            {
                if (brederMaker == 'u')
                {
                    breedte++;
                    Console.WriteLine(headers[0].PadRight(breedte) + headers[1].PadRight(breedte) + headers[2].PadRight(breedte) + headers[3].PadRight(breedte) + headers[4].PadRight(breedte) + headers[5].PadRight(breedte));
                    Console.WriteLine(categorie[0].PadRight(breedte) + artikel[0].PadRight(breedte) + merk[0].PadRight(breedte) + prijs[0].PadRight(breedte) + stock[0].PadRight(breedte) + nuBesteld[0].PadRight(breedte));
                    Console.WriteLine(categorie[1].PadRight(breedte) + artikel[1].PadRight(breedte) + merk[1].PadRight(breedte) + prijs[1].PadRight(breedte) + stock[1].PadRight(breedte) + nuBesteld[1].PadRight(breedte));
                    Console.WriteLine(categorie[2].PadRight(breedte) + artikel[2].PadRight(breedte) + merk[2].PadRight(breedte) + prijs[2].PadRight(breedte) + stock[2].PadRight(breedte) + nuBesteld[2].PadRight(breedte));
                }
                brederMaker = Convert.ToChar(Console.Read());
            }
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region SchrijfEenKruis
            int ySchrijver = 50;
            for (int tellerX = 0; tellerX < 50; tellerX++)
            {
                Console.SetCursorPosition(tellerX, tellerX);
                Console.WriteLine("x");
            }
            for (int tellerY = 0; tellerY < 50; tellerY++)
            {
                ySchrijver--;
                Console.SetCursorPosition(tellerY, ySchrijver);
                Console.WriteLine("x");
            }
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region SchrijfEenVierkant
            Console.WriteLine("Geef het startpunt in X dimensie");
            int puntX = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Geef het startpunt in Y dimensie");
            int puntY = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Hoe breed en lang moet het vierkant zijn?");
            int dimensie = Convert.ToInt16(Console.ReadLine());
            Console.Clear();
            Console.SetCursorPosition(puntX, puntY);
            for (int teller = 0; teller < dimensie + 1; teller++)
            {
                Console.WriteLine("x");
                puntX++;
                Console.SetCursorPosition(puntX, puntY);
            }
            Console.WriteLine("x");
            puntX = puntX - dimensie - 1;
            Console.SetCursorPosition(puntX, puntY);
            for (int teller = 0; teller < dimensie + 1; teller++)
            {
                Console.WriteLine("x");
                puntY++;
                Console.SetCursorPosition(puntX, puntY);
            }
            for (int teller = 0; teller < dimensie + 1; teller++)
            {
                Console.WriteLine("x");
                puntX++;
                Console.SetCursorPosition(puntX, puntY);
            }
            puntY = puntY - dimensie - 1;
            for (int teller = 0; teller < dimensie + 1; teller++)
            {
                Console.WriteLine("x");
                puntY++;
                Console.SetCursorPosition(puntX, puntY);
            }
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion

            Console.ReadKey();
        }
    }

    class Program_Scott
    {
        static void rechthoek(string[] args)
        {
            int i;
            int b = 10;
            for (i = 11; i <= 19; i++)
            {
                Console.SetCursorPosition(i, b);
                Console.Write("0");
            }
            int j;
            int c = 20;
            for (j = 11; j <= 19; j++)
            {
                Console.SetCursorPosition(j, c);
                Console.Write("0");
            }
            int k;
            int d = 10;
            for (k = 10; k <= 20; k++)
            {
                Console.SetCursorPosition(d, k);
                Console.Write("0");
            }
            int l;
            int e = 20;
            for (l = 10; l <= 20; l++)
            {
                Console.SetCursorPosition(e, l);
                Console.Write("0");
            }
        }
        static void kruis(string[] args)
        {
            int i;
            for (i = 0; i <= 20; i++)
            {
                Console.Write("0");
                Console.SetCursorPosition(i, i);
            }
            int j;
            int k = 0;
            for (j = 20; j >= 0; j--, k++)
            {
                Console.Write("0");
                Console.SetCursorPosition(j, k);
            }
        }

    }

    class Program_Abdulla
    {
        static void Main(string[] args)
        {
            // pagina 192
            // 1-2-3-5-6-7-8-9-10

            // 1. Write a program that reads from the console three numbers of type int and prints their sum.

            //int[] getallen = new int[3];
            //int som = 0;
            //Console.WriteLine("Geef 3 getallen in: ");
            //for (int i = 0; i < getallen.Length; i++)
            //{
            //    getallen[i]=int.Parse(Console.ReadLine());
            //    som += getallen[i];
            //}
            //Console.WriteLine($"De som van deze getallen is {som}.");


            // 2. Write a program that reads from the console the radius "r" of a circle and prints its perimeter and area.

            //decimal r, omtrek, oppervlakte;
            //Console.Write("Geef een straalwaarde in: ");
            //r = decimal.Parse(Console.ReadLine());
            //omtrek = Math.Round(2m * (decimal)Math.PI * r * 100) / 100;
            //oppervlakte = Math.Round(r * r * (decimal)Math.PI * 100) / 100;
            //Console.WriteLine($"De cirkel heeft een omtrek van {omtrek} en een oppervlakte van {oppervlakte}");


            // 3. A given company has name, address, phone number, fax number, website and manager.The manager has name, surname and phone number.
            // Write a program that reads information about the company and its   
            // manager and then prints it on the console.

            //string[] bedrijfInfo = new string[5];
            //string[] managerInfo = new string[3];
            //ReadDetails(bedrijfInfo, managerInfo);
            //PrintDetails(bedrijfInfo, managerInfo);

            //static void ReadDetails(string[] bedrijfInfo, string[] managerInfo)
            //{
            //    Console.WriteLine("Company details");
            //    Console.Write("Company name: ");
            //    bedrijfInfo[0] = Console.ReadLine();
            //    Console.Write("Address: ");
            //    bedrijfInfo[1] = Console.ReadLine();
            //    Console.Write("Phone number: ");
            //    bedrijfInfo[2] = Console.ReadLine();
            //    Console.Write("Fax number: ");
            //    bedrijfInfo[3] = Console.ReadLine();
            //    Console.Write("Website: ");
            //    bedrijfInfo[4] = Console.ReadLine();

            //    Console.WriteLine();

            //    Console.WriteLine("Manager details");
            //    Console.Write("Name: ");
            //    managerInfo[0] = Console.ReadLine();
            //    Console.Write("Surname: ");
            //    managerInfo[1] = Console.ReadLine();
            //    Console.Write("Phone: ");
            //    managerInfo[2] = Console.ReadLine();
            //    Console.WriteLine();
            //}

            //static void PrintDetails(string[] bedrijfInfo, string[] managerInfo)
            //{
            //    Console.WriteLine("Company details");
            //    Console.WriteLine($"Company name: {bedrijfInfo[0]}");
            //    Console.WriteLine($"Address: {bedrijfInfo[1]}");
            //    Console.WriteLine($"Phone number: {bedrijfInfo[2]}");
            //    Console.WriteLine($"Fax number: {bedrijfInfo[3]}");
            //    Console.WriteLine($"Website: {bedrijfInfo[4]}");

            //    Console.WriteLine();

            //    Console.WriteLine("Manager details");
            //    Console.WriteLine($"Name: {managerInfo[0]}");
            //    Console.WriteLine($"Surname: {managerInfo[1]}");
            //    Console.WriteLine($"Phone: {managerInfo[2]}");
            //}


            // 5. Write a program that reads from the console two integer numbers (int) and prints how many numbers between them exist, such that the remainder of their division by 5 is 0. Example: in the range (14, 25) there are 3 such numbers: 15, 20 and 25.

            //int getal1, getal2;
            //Console.Write("Geef een getal in: ");
            //getal1 = int.Parse(Console.ReadLine());
            //Console.Write("Geef nog een getal in: ");
            //getal2 = int.Parse(Console.ReadLine());

            //Console.WriteLine($"De volgende getallen zitten tussen {getal1} en {getal2} en zijn deelbaar door 5:");
            //for (int i = getal1; i <= getal2; i++)
            //{
            //    if (i % 5 == 0)
            //        Console.Write($"{i} ");
            //}
            //Console.WriteLine();


            // 6. Write a program that reads two numbers from the console and prints the greater of them. Solve the problem without using conditional statements.

            //int getal1, getal2;
            //Console.Write("Geef een getal in: ");
            //getal1 = int.Parse(Console.ReadLine());
            //Console.Write("Geef nog een getal in: ");
            //getal2 = int.Parse(Console.ReadLine());

            //Console.WriteLine($"{(getal1 >= getal2 ? getal1 : getal2)} is het grootste getal.");


            // 7. Write a program that reads five integer numbers and prints their sum. If an invalid number is entered the program should prompt the user to enter another number.

            //int[] getallen = new int[5];
            //int sum=0;
            //Console.WriteLine("Geef 5 getallen in:");
            //for (int i = 0; i < getallen.Length; i++)
            //{
            //    string stringGetal;
            //    int getal;
            //    bool isInteger;

            //    do
            //    {
            //        stringGetal = Console.ReadLine();
            //        isInteger = int.TryParse(stringGetal, out getal);
            //        if(!isInteger)
            //            Console.WriteLine("Dit is geen getal, probeer opnieuw.");
            //    } while (!isInteger);

            //    getallen[i] = getal;
            //    sum += getal;
            //}
            //Console.WriteLine($"De som van de getallen is {sum}");


            // 8. Write a program that reads five numbers from the console and prints the greatest of them.

            //int invoerGetal, grootsteGetal;
            //Console.WriteLine("Geef 5 getallen in: ");
            //grootsteGetal = int.Parse(Console.ReadLine());

            //for (int i = 0; i < 4; i++)
            //{
            //    invoerGetal = int.Parse(Console.ReadLine());
            //    if (invoerGetal > grootsteGetal)
            //        grootsteGetal = invoerGetal;
            //}
            //Console.WriteLine($"{grootsteGetal} is het grootste getal.");


            // 9. Write a program that reads an integer number n from the console. After that reads n numbers from the console and prints their sum.
            //int n;
            //int sum = 0;
            //Console.WriteLine("Hoeveel getallen wil je ingeven?");
            //n = int.Parse(Console.ReadLine());
            //Console.WriteLine($"Geef {n} getallen in: ");
            //for (int i = 0; i < n; i++)
            //{
            //    sum += int.Parse(Console.ReadLine());
            //}
            //Console.WriteLine($"De som van alle getallen is {sum}.");


            // 10. Write a program that reads an integer number n from the console and prints all numbers in the range [1…n], each on a separate line.
            //int n;
            //Console.Write("Geef een getal in: ");
            //n = int.Parse(Console.ReadLine());
            //for (int i = 1; i <= n; i++)
            //{
            //    Console.WriteLine(i);
            //}

        }

    }

    class Program_Koen
    {
        static void ConsoleInAndOut()
        {
            //1. Write a program that reads from the console three numbers of type int and prints their sum.
            int inputNumber = 0;
            int sum = 0;

            Console.WriteLine("We bereken de som van 3 cijfers.");
            Console.Write("Eerste getal: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out inputNumber);
            Console.ForegroundColor = ConsoleColor.White;
            sum += inputNumber;

            Console.Write("Tweede getal: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out inputNumber);
            Console.ForegroundColor = ConsoleColor.White;
            sum += inputNumber;

            Console.Write("Derde getal: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out inputNumber);
            Console.ForegroundColor = ConsoleColor.White;
            sum += inputNumber;

            Console.WriteLine($"De som van de 3 getallen is {sum}.");

            Console.WriteLine("----------");

            //2. Write a program that reads from the console the radius "r" of a circle and prints its perimeter and area.
            Console.Write("We bereken de omtrek en grootte van een circkel. Radius : ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out inputNumber);
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine($"De omtrek van de cirkel is {2 * inputNumber * Math.PI:#.00} en de oppervlakte is {inputNumber * inputNumber * Math.PI:#.00}.");

            Console.WriteLine("----------");

            //3. A given company has name, address, phone number, fax number, web
            //   site and manager.The manager has name, surname and phone number.
            //   Write a program that reads information about the company and its
            //   manager and then prints it on the console.
            string companyName = "";
            string companyAdress = "";
            int companyPhoneNumbr = 0;
            int companyFaxNumbr = 0;
            string companySite = "";

            string managerName = "";
            string managerSurname = "";
            int managerPhoneNumber = 0;

            string numbersToFormat = "";

            //Lot of double code, should make functions for those
            Console.WriteLine("Bedrijf gegevens");
            Console.Write("Bedrijfs naam: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            companyName = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Bedrijfs adres: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            companyAdress = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Bedrijfs telefoon nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            numbersToFormat = Console.ReadLine();
            numbersToFormat = numbersToFormat.Replace(" ", string.Empty);
            Int32.TryParse(numbersToFormat, out companyPhoneNumbr);
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Bedrijfs fax nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            numbersToFormat = Console.ReadLine();
            numbersToFormat = numbersToFormat.Replace(" ", string.Empty);
            Int32.TryParse(numbersToFormat, out companyFaxNumbr);
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Bedrijfs site: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            companySite = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Manager gegevens");
            Console.Write("Manager voornaam: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            managerName = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Manager achternaam: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            managerSurname = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Manager telefoon nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            numbersToFormat = Console.ReadLine();
            numbersToFormat = numbersToFormat.Replace(" ", string.Empty);
            Int32.TryParse(numbersToFormat, out managerPhoneNumber);
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine($"Het bedrijf {companyName} bevind zich op {companyAdress}.\nJe kan heb berijken via telefoon op {companyPhoneNumbr:0### ## ## ##} of via fax op {companyFaxNumbr:0### ## ## ##}.\nBezoek zeker ook eens hun site: {companySite}.");
            Console.WriteLine($"De manager van het bedrijf is {managerName + " " + managerSurname}, bij vragen kan je hem/haar bereiken op {managerPhoneNumber:0### ## ## ##}");

            Console.WriteLine("----------");

            //5. Write a program that reads from the console two integer numbers (int)
            //   and prints how many numbers between them exist, such that the
            //   remainder of their division by 5 is 0.Example: in the range(14, 25)
            //   there are 3 such numbers: 15, 20 and 25.

            int numberOne = 0;
            int numberTwo = 0;

            Console.WriteLine("Geef 2 nummers in en we berekenen welke deelbaar zijn door 5.");
            Console.Write("Eerste nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out numberOne);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Tweede nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out numberTwo);
            Console.ForegroundColor = ConsoleColor.White;

            if (numberOne > numberTwo)
            {
                int temp = numberOne;
                numberOne = numberTwo;
                numberTwo = temp;
            }

            Console.WriteLine("Deze getallen zijn deelbaar door 5: ");

            for (int i = numberOne; i <= numberTwo; i++)
            {
                if ((i % 5) == 0)
                    Console.Write(i + " ");
            }
            Console.WriteLine("");

            Console.WriteLine("----------");

            //6. Write a program that reads two numbers from the console and prints the
            //   greater of them. Solve the problem without using conditional statements.
            Console.WriteLine("Geef 2 nummers in we kijken welke het grootste is.");

            Console.Write("Eerste nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out numberOne);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Tweede nummer: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out numberTwo);
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine($"Het grootste nummer is {(numberOne + numberTwo + Math.Abs(numberOne - numberTwo)) / 2}."); //formule van het boek

            Console.WriteLine("----------");

            //7. Write a program that reads five integer numbers and prints their
            //   sum.If an invalid number is entered the program should prompt the user
            //   to enter another number.
            Console.WriteLine("Geef 5 nummers in, we berekenen de som.");
            sum = 0;

            for (int i = 0; i < 5; i++)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                while (!Int32.TryParse(Console.ReadLine(), out numberOne))
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("This is not a number!");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }

                sum += numberOne;
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"De som van de 5 nummers is {sum}.");

            Console.WriteLine("----------");

            //8. Write a program that reads five numbers from the console and prints the
            //   greatest of them.
            Console.WriteLine("Geef 5 nummers in, we printen de grootste.");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out numberOne);
            numberTwo = numberOne;

            for (int i = 0; i < 4; i++)
            {
                Int32.TryParse(Console.ReadLine(), out numberOne);
                if (numberOne > numberTwo)
                    numberTwo = numberOne;
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"{numberTwo} is de grootste van de 5 nummer.");

            Console.WriteLine("----------");

            //10. Write a program that reads an integer number n from the console and
            //    prints all numbers in the range[1…n], each on a separate line.
            Console.Write("Geef een getal in, we printen elk getal van 1 tot jou getal. Jouw getal: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Int32.TryParse(Console.ReadLine(), out numberOne);
            Console.ForegroundColor = ConsoleColor.White;

            for (int i = 1; i <= numberOne; i++)
            {
                Console.WriteLine(i);
            }
        }
    }

    class Program_Niels
    {
        static void kruis(string[] args)
        {
            Console.WriteLine("How long should the diagonals of the cross be?");
            int crossSize = Int32.Parse(Console.ReadLine());
            //Console.WriteLine(crossSize);
            int startPositionX = 0;


            Console.Clear();

            #region teveel nagedacht
            // int evenOfOneven = crossSize % 2;           //check voor even/oneven want bij oneven krijgen we een heel lelijk kruis



            //if (evenOfOneven == 0)
            //{
            #endregion

            for (int i = 0; i < crossSize; i++)
            {
                Console.SetCursorPosition(startPositionX, i);
                Console.WriteLine("o");
                startPositionX += 2;                //ik gebruik +2 voor x coordinaat, ziet er beter uit
            }

            int secondLineStartX = crossSize * 2 - 2;   //moet bovenaan staan

            for (int i = 0; i < crossSize; i++)
            {
                Console.SetCursorPosition(secondLineStartX, i);
                Console.WriteLine("o");
                secondLineStartX -= 2;
            }

            #region teveel nagedacht
            //}

            //else
            //{
            //    for (int i = 0; i < crossSize; i++)
            //    {
            //        Console.SetCursorPosition(startPositionX, i);
            //        Console.WriteLine("o");
            //        startPositionX += 2;
            //    }

            //    int secondLineStartX = crossSize * 2 - 2;

            //    for (int i = 0; i < crossSize; i++)
            //    {
            //        Console.SetCursorPosition(secondLineStartX, i);
            //        Console.WriteLine("o");
            //        secondLineStartX -= 2;
            //    }
            //}
            #endregion
        }

        static void Rechthoek(string[] args)
        {
            Console.WriteLine("How long should the horizontal side be?");
            int horizontalSide = Int32.Parse(Console.ReadLine());
            Console.WriteLine("How long should the vertical side be?");
            int verticalSide = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Please enter starting X-coordinate (top left corner of the rectangle):");
            int startPointX = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Please enter starting Y-coordinate (top left corner of the rectangle):");
            int startPointY = Int32.Parse(Console.ReadLine());

            int lineOneX = startPointX * 2;                               //horizontale bovenste lijn //*2 omdat de coordinaten in console niet proportioneel zijn
            int lineOneY = startPointY;

            int lineTwoX = startPointX * 2;                               //verticale linkse lijn
            int lineTwoY = startPointY;

            int lineThreeX = startPointX * 2;                             //horizontale onderste lijn
            int lineThreeY = startPointY + verticalSide - 1;            //-1 omdat je anders onder de verticale lijn begint, bvb startPointY = 10 en verticalSide = 4 dan teken je lineTwo op Y coordinaten 10 11 12 13 en begin je lineThree op 10 + 4 = 14, dan zou de horizontale lijn plots 5 worden ipv 4

            int lineFourX = startPointX * 2 + horizontalSide * 2 - 2;       //verticale linkse lijn // *2 omdat ik +2 doe bij x coordinaten voor een proportioneel resultaat // -2 om dezelfde reden als hierboven
            int lineFourY = startPointY;



            Console.Clear();

            for (int i = 0; i < horizontalSide; i++)
            {
                Console.SetCursorPosition(lineOneX, lineOneY);
                Console.Write("o");
                lineOneX += 2;                                     //+1 geeft geen proportionele verhouding met regelhoogte             
            }


            for (int i = 0; i < verticalSide; i++)
            {
                Console.SetCursorPosition(lineTwoX, lineTwoY);
                Console.WriteLine("o");
                lineTwoY++;
            }

            for (int i = 0; i < horizontalSide; i++)
            {
                Console.SetCursorPosition(lineThreeX, lineThreeY);
                Console.WriteLine("o");
                lineThreeX += 2;
            }

            for (int i = 0; i < verticalSide; i++)
            {
                Console.SetCursorPosition(lineFourX, lineFourY);
                Console.WriteLine("o");
                lineFourY++;
            }
        }
    }

    class Program_Dennis
    {
        public static void cross()
        {
            int x = 0;
            int y = 0;
                
            for (x = 0; x < 20; x++)
            {
                Console.SetCursorPosition(x, y++);
                Console.Write("o");
            }

            y = 0;
            for (x = 20; x > 0; x--)
            {
                Console.SetCursorPosition(x, y++);
                Console.Write("o");
            }
        }

        public static void circle()
        {
            int xCenter = 20;
            int yCenter = 20;

            int radius = 10;
            int angle = 0;

            int x, y;

            while (angle < 360)
            {
                double radiant = Math.PI * angle / 180.0;
                x = xCenter + Convert.ToInt32(Math.Sin(radiant) * radius);
                y = yCenter - Convert.ToInt32(Math.Cos(radiant) * radius);
                Console.SetCursorPosition(x, y);
                Console.Write("o");
                angle++;
                System.Threading.Thread.Sleep(5);
            }
        }

        public static void square()
        {
            int xStart = 10;
            int yStart = 10;
            int size = 10;

            int y = yStart;
            int x = xStart;
            for (x = xStart; x < xStart + size; x++)
            {
                Console.SetCursorPosition(x, y);
                Console.Write("o");
            }

            y = yStart + size;
            for (x = xStart; x < xStart + size + 1; x++)
            {
                Console.SetCursorPosition(x, y);
                Console.Write("o");
            }

            x = xStart;
            for (y = yStart; y < yStart + size; y++)
            {
                Console.SetCursorPosition(x, y);
                Console.Write("o");
            }

            x = xStart + size;
            for (y = yStart; y < yStart + size; y++)
            {
                Console.SetCursorPosition(x, y);
                Console.Write("o");
            }
        }
    }
}
