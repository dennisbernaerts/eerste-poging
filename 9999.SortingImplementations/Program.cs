﻿using System;

namespace _9999.SortingImplementations
{
    class Program
    {
        static void Main(string[] args)
        {
            //Genereer Arr
            Console.WriteLine("Hoeveel getallen wil je genereren");
            int values = 10;
            int.TryParse(Console.ReadLine(), out values);
            int[] array = new int[values];

            Klas.FillArray(array);
            Klas.PrintArraySingleLine( array);

            int[] sortedArray = Klas.Sort(array, Klas.KlasLeden.Koen);

            Klas.PrintArraySingleLine( sortedArray, ConsoleColor.White);
        }
    }

    public class SortingMethods
    {
        public static int[] Sort(int[] arrayToSort, string name)
        {
            //this part you code yourself
            return new int[10];
        }
    }


    static class Klas
    {
        public enum KlasLeden
        {
            Koen,
            Abdullah,
            Dennis,
            Hilde,
            Joren,
            Kris,
            Manal,
            Nick,
            Patrick,
            Stijn,
            Thomas
        }
        public static void FillArray(int[] arrRef)
        {
            Random r = new Random();

            for (int i = 0; i < arrRef.Length; ++i)
            {
                arrRef[i] = r.Next(0, 100);
            }
        }

        public static void PrintArray(int[] arrRef, ConsoleColor color = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = color;
            foreach (int value in arrRef)
            {
                Console.WriteLine(value);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void PrintArraySingleLine(int[] arrRef, ConsoleColor color = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = color;
            for (int i = 0; i < arrRef.Length; ++i)
            {
                Console.Write(arrRef[i]);
                if (i < (arrRef.Length - 1))
                    Console.Write(", ");
                else
                    Console.Write("\n");

            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static int[] Sort(int[] arrayToSort, KlasLeden name)
        {
            //Deep copy maken van array, anders is de originele array ook aangepast
            //Arrays worden doorgeven als shallow copy? Mss iets te maken dat er enkel een nieuwe pointer word gemaakt ipv de volledige array te kopieren
            //Pointer wijst nog naar originele array waardoor 'beide' arrays aangepast worden (?)
            int[] arrayToReturn = new int[arrayToSort.Length];
            Array.Copy(arrayToSort, arrayToReturn, arrayToReturn.Length);

            //Sorting functions
            switch (name)
            {
                case KlasLeden.Koen:
                    bool swapped = true;
                    while (swapped)
                    {
                        swapped = false;
                        for (int i = 0; i < (arrayToReturn.Length - 1); ++i)
                        {
                            int numberToSwap = 0;
                            if (arrayToReturn[i] > arrayToReturn[i + 1])
                            {
                                numberToSwap = arrayToReturn[i];
                                arrayToReturn[i] = arrayToReturn[i + 1];
                                arrayToReturn[i + 1] = numberToSwap;
                                swapped = true;
                            }
                        }
                    }
                    break;
                default:
                    WriteError("Persoon zit niet in de klas!");
                    break;
            }

            return arrayToReturn;
        }


        public static void WriteError(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}

