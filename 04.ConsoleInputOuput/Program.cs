﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04.ConsoleInputOuput
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }




    class Program_Vic
    {
        static void run(string[] args)
        {
            #region Oef1 Neem 3 getallen en print de som.
            int eersteGetal = Convert.ToInt32(Console.ReadLine());
            int tweedeGetal = Convert.ToInt32(Console.ReadLine());
            int derdeGetal = Convert.ToInt32(Console.ReadLine());
            int som = eersteGetal + tweedeGetal + derdeGetal;
            Console.WriteLine(som);
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 2 Bereken de omtrek en oppervlakte van een cirkel. GEBRUIK VAN MATH.PI
            double radius = double.Parse(Console.ReadLine());
            double perimeter = (radius * 2) * Math.PI;
            double area = Math.PI * (radius * radius);
            Console.WriteLine("The perimeter equals {0} and the area is {1}", perimeter, area);
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 3 Slaag informatie op over bedrijf en hun manager.
            Console.WriteLine("Give company name");
            string companyName = Console.ReadLine();
            Console.WriteLine("Give fax number");
            string faxNumber = (Console.ReadLine());
            Console.WriteLine("Give phone number of the company");
            string phoneNumber = (Console.ReadLine());
            Console.WriteLine("Name of company website?");
            string website = Console.ReadLine();
            Console.WriteLine("Give name, surname, and telephone number of the manager");
            string[] manager = { Console.ReadLine(), Console.ReadLine(), Console.ReadLine() };

            Console.WriteLine(companyName);
            Console.WriteLine(phoneNumber);
            Console.WriteLine(faxNumber);
            Console.WriteLine(website);
            Console.WriteLine("{0} {1} {2}", manager[0], manager[1], manager[2]);

            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 5 Geef 2 getallen in, en kijk welke getallen er tussen zitten die deelbaar zijn door 5. GEBRUIK VAN MODULUS
            Console.WriteLine("Geef een eerste getal in");
            int eersteGetalA = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef een tweede getal in");
            int tweedeGetalB = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("De volgende getallen tussen {0} en {1} zijn deelbaar door 5!", eersteGetal, tweedeGetal);
            for (int teller = eersteGetal; teller < tweedeGetal + 1; teller++)
            {
                if (teller % 5 == 0)
                {
                    Console.WriteLine(teller);
                }
            }
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 6 Geef 2 getallen in en kijk welke groter is. GEBRUIK VAN MATH.MAX
            Console.WriteLine("Geef een getal in.");
            int getalA = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef een tweede getal in.");
            int getalB = Convert.ToInt32(Console.ReadLine());
            int grootste = Math.Max(getalA, getalB);
            Console.WriteLine("Het grootste getal is {0}", grootste);
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 7 Druk de som van 5 integere getallen in. GEBRUIK VAN TRYPARSE
            Console.WriteLine("Geef vijf integere getallen in.");
            //long som = 0;
            for (int i = 0; i < 6; i++)
            {
                int temp;
                bool getalGoed = int.TryParse(Console.ReadLine(), out temp);
                while (getalGoed == false)
                {
                    getalGoed = int.TryParse(Console.ReadLine(), out temp);
                }
                som = som + temp;
            }
            Console.WriteLine("De som van de integere getallen is {0}", som);
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 8 Geef 5 getallen in, kijk welke het grootste is. GEBRUIK VAN MATH.MAX
            Console.WriteLine("Geef 5 getallen in.");
            double ingegevenGetal = Convert.ToInt32(Console.ReadLine());
            for (int teller = 0; teller < 5; teller++)
            {
                ingegevenGetal = Math.Max(Convert.ToInt32(Console.ReadLine()), ingegevenGetal);
            }
            Console.WriteLine("Het hoogste getal is {0}", ingegevenGetal);
            Console.WriteLine("-------------------------------------------------------------------");
            #endregion
            #region Oef 9 Kijk na of getal integer is, print dan af. GEBRUIK VAN TRYPARSE
            int getal;
            Console.WriteLine("Geef een getal in");
            bool ingegevenGetalChecker = int.TryParse(Console.ReadLine(), out getal);
            while (ingegevenGetalChecker != true)
            {
                Console.WriteLine("Fout. U moet een integer getal ingeven");
                ingegevenGetalChecker = int.TryParse(Console.ReadLine(), out getal);
            }
            for (int teller = 0; teller < getal + 1; teller++)
            {
                Console.Write("{0} ", teller);
                if (teller % 10 == 0)
                {
                    Console.WriteLine();
                }
            }
            #endregion






            Console.ReadKey();
        }
    }

    class Program_abdul
    {
        static void run(string[] args)
        {
            string line = string.Format("{0:C2}", 123.456);
            char[] lineCharArray = line.ToCharArray();
            byte[] lineByteArray = Encoding.ASCII.GetBytes(lineCharArray);


            foreach (byte b in lineByteArray)
            {
                Console.WriteLine("0x{0:X2}", b);
            }
            Console.WriteLine();
        }

    }
}
