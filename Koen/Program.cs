﻿using System;

namespace Ex.WatIsJeNaam
{

    class Program
    {
        static void Main(string[] args)
        {
            //Program_Abdul.run(args);
            Program_Hilde.run(args);

        }
    }

    static class Program_Abdul
    {
        public static void run(string[] args)
        {
            string naam;
            Console.Write("Toets uw naam in: ");
            naam = Console.ReadLine();

            Console.WriteLine($"Dag {naam}!");
            ConsoleKeyInfo knop = Console.ReadKey();

            while (knop.KeyChar != 'q')
            {
                if (knop.KeyChar != 'r')
                {
                    Console.Write("Toets uw naam in: ");
                    naam = Console.ReadLine();
                    Console.WriteLine($"Dag {naam}!");
                }
                knop = Console.ReadKey();
            }
        }
    }

    static class Program_Scott
    {
        public static void run(string[] args)
        {
            System.Console.Write("Wat is je naam?");
            string userName = Console.ReadLine();
            System.Console.WriteLine("Hallo" + userName);

            char key = (char)Console.Read();

            if (key == 'Q')
            {
                System.Console.WriteLine("Einde programma");
            }
            if (key == 'R')
            {
                System.Console.Write("Wat is je naam?");
                userName = Console.ReadLine();
                System.Console.WriteLine("Hallo" + userName);
            }
        }
    }

    static class Program_Hilde
    {
        public static void run(string[] args)
        {
            string testString;
            Console.Write("Wat is je naam?");
            testString = Console.ReadLine();
            Console.WriteLine("Goedemorgen, {0}", testString);

            if (Console.ReadKey().Key == ConsoleKey.R)
            {
                string naam;
                Console.Write("Wat is je naam?");
                naam = Console.ReadLine();
                Console.WriteLine("Goedemorgen, {0}", testString);
            }
        }
    }

    static class Program_Stijn
    {
        public static void run(string[] args)
        {
            /*ZONDER METHODES
             * 
             * Console.Write("Wat is jouw naam: ");
            string naam = Console.ReadLine();

            Console.WriteLine($"Dag {naam}, hoe gaat het met je?");

            Console.WriteLine("---------------");
            Console.WriteLine("Duw Q voor te stoppen en R om opnieuw te gaan");
            char inputChar = Console.ReadKey().KeyChar;

            while (inputChar != 'q' && inputChar != 'r')
            {
                Console.WriteLine("\nGelieve q of r te kiezen");
                inputChar = Console.ReadKey().KeyChar;
            }

            if (inputChar == 'q')
            {
                Environment.Exit(0);
            }
            else if (inputChar == 'r')
            {
                Console.Write("Wat is jouw naam: ");
                naam = Console.ReadLine();
                Console.WriteLine($"Dag {naam}, hoe gaat het met je?");
            }*/

            ///Met methodes

            AskName();
            string naam = Console.ReadLine();
            Greet(naam);

            Console.WriteLine("---------------");
            Console.WriteLine("Duw Q voor te stoppen en R om opnieuw te gaan");
            char inputChar = Console.ReadKey().KeyChar;

            while (inputChar != 'q' && inputChar != 'r')
            {
                Console.WriteLine("\nGelieve q of r te kiezen");
                inputChar = Console.ReadKey().KeyChar;
            }

            if (inputChar == 'q')
            {
                Environment.Exit(0);
            }
            else if (inputChar == 'r')
            {
                AskName();
                naam = Console.ReadLine();
                Greet(naam);
            }

        }

        static void AskName()
        {
            Console.Write("Wat is jouw naam: ");
        }

        static void Greet(string naam)
        {
            Console.WriteLine($"Dag {naam}, hoe gaat het met je?");
        }
    }

    static class Program_Thomas
    {
        public static void run(string[] args)
        {
            //DENNIS : hoe kunnen we deze dubbele code vermijden ?

            string naam = AskName();
            GreetName(naam);
            string letter = Console.ReadLine();
            while (letter != "q")
            {
                if (letter == "r")
                {
                    AskName();
                    GreetName(naam);
                }
                else
                {
                    Console.WriteLine("Error kies tussen q of r!");
                    Console.WriteLine("Type q of r");
                    letter = Console.ReadLine();
                }
            }
        }

        static string AskName()
        {
            Console.WriteLine("Wat is je naam?");
            string naam = Console.ReadLine();

            return naam;
        }

        static void GreetName(string naam)
        {
            Console.WriteLine("Goede morgen " + naam);
            Console.WriteLine("Type q of r");
        }
    }

    static class Program_Kris
    {
        public static void run()
        {
            string naam;
            string key = "r";

            // creatief opgelost
            Console.WriteLine("Wat is je naam?");
            naam = Console.ReadLine();
            Console.WriteLine("Hallo " + naam + " hoe gaat het?");

            Console.WriteLine("Druk op R om opnieuw te gaan of druk op Q om te stoppen");
            key = Convert.ToString(Console.ReadKey());

            if (key == "r")
            {
                run();
            }
            else if (key == "q")
            {

            }
            else
            {
                Console.WriteLine("Foute ingave... programma wordt toch gesloten, tot ziens" + naam);
            }
        }


    }

    static class Program_Joren
    {
        //DENNIS : mooi die commentaren erbij. En ook mooie loop structuur !
        public static void run(string[] args)
        {
            // Zo lang asking true is, zal het programma je naam blijven vragen.
            bool asking = true;
            while (asking)
            {
                // Vraag de naam en print deze.
                Console.WriteLine("Wat is je naam?");
                Console.WriteLine("Hey, " + Console.ReadLine() + "!");

                // Zo lang validInput false is, zal het programma input blijven vragen
                bool validInput = false;
                while (!validInput)
                {
                    Console.WriteLine("Duw op [R] om te herstarten, [Q] om te stoppen.");
                    ConsoleKey inputKey = Console.ReadKey().Key;

                    // 'Q' is ingegeven -> Stop programma volledig
                    if (inputKey == ConsoleKey.Q)
                    {
                        asking = false;
                        validInput = true;
                    }

                    // Niet 'Q' en niet 'R' -> invalid input
                    else if (inputKey != ConsoleKey.R)
                    {
                        asking = false;
                        validInput = false;
                        Console.Clear();
                        Console.WriteLine("Ongeldige input.");
                    }

                    // 'R' is ingegeven -> Restart
                    else
                    {
                        asking = true;
                        validInput = true;
                    }
                }

                // We clearen het console window om het wat properder te maken.
                Console.Clear();
            }
        }
    }

    static class Program_Niels
    {
        public static void run(string[] args)
        {

            System.Console.Write("Wat is je naam?");
            string naam = System.Console.ReadLine();

            System.Console.WriteLine("Dag " + naam + ", hoe gaat het?");

            System.ConsoleKeyInfo letter = System.Console.ReadKey();


            /*
            if letter == 'q';
            {
                System.Environment.Exit(1);
            }
                else if letter == 'r';
            {
                Application.Restart();
            }
                else
            {
                return;
            }
            */

        }
    }

    static class Program_Koen
    {
        public static void run(string[] args)
        {
            ReadWriteName();

            var keyStroke = Console.ReadKey(true).KeyChar;
            while (keyStroke != 'Q' && keyStroke != 'q') //KeyStroke kan ook naar capital gezet worden
            {
                if (keyStroke == 'R' || keyStroke == 'r')
                {
                    ReadWriteName();
                }
                keyStroke = Console.ReadKey(true).KeyChar;
            }
        }

        static void ReadWriteName()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Dag gebruiken, wat is jou naam? ");
            Console.ForegroundColor = ConsoleColor.White;
            string name = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Dag {name}");
            Console.ForegroundColor = ConsoleColor.White;
        }

    }

    static class Program_Vic
    {
        public static void run(string[] args)
        {
            Console.WriteLine("Hallo, wat is uw naam?"); string NaamUser = Console.ReadLine();
            if (NaamUser == "Q")   //(NaamUser == 'Q')
            {
            }
            Console.WriteLine("Dag " + NaamUser + "!");
        }
    }

    static class Program_Patrick
    {
        public static void run(string[] args)
        {
            string naam = "";
            Console.Write("Wat is je naam? ");
            naam = Console.ReadLine();
            Console.WriteLine("Dag " + naam);
            Console.WriteLine();
            Console.WriteLine("Druk op een toets (Q= einde/R= nog naam vragen");
            Console.WriteLine();

            System.ConsoleKeyInfo keyinfo = System.Console.ReadKey(); //gebruiker drukt op een toets
            Console.WriteLine();

            //DENNIS : if (Char.ToUpper(keyinfo.KeyChar) == 'Q')
            if ((keyinfo.KeyChar == 'Q') || (keyinfo.KeyChar == 'q'))
            { Console.WriteLine("Einde Programma"); }
            //Q stopt app ,R = nog eens vragen
            else
            {
                if ((keyinfo.KeyChar == 'R') | (keyinfo.KeyChar == 'r'))
                {
                    Console.Write("Geef je naam nog een keer in? ");
                    naam = Console.ReadLine();
                    Console.WriteLine("Nogmaals Dag " + naam);


                }
            }

        }

    }

}

