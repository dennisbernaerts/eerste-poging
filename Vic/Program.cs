﻿using System;

namespace Ex.Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            Program_Hilde.run(args);
        }
    }

    class Program_Hilde
    {
        public static void run(string[] args)
        // HELP: waarvoor heb je static void run, static int[] Sort en static void PrintArray in aparte methods gezet? 
        // Wat is daar het nut van? Ik vermoed dat dit conventie is en dat je declaratie, berekening en afdrukken apart moet noteren?
        // Maar als ik mijn uitschrijf-loop naar PrintArray method verplaats, werkt het programma niet meer, heeft dit te maken met die referenties? 
        {

            int[] originalArray = new int[10] { 5, 7, 3, 5, 8, 2, 5, 3, 7, 5 };
            int[] sortedArray = new int[10];

            for (int i = 0; i <= 9 ; i++)
            {
                sortedArray[i] = originalArray[i];
            }

            Sort(sortedArray);


        }

        static int[] Sort(int[] array)
        {
            int[] sortedArray = new int[10];
            //sort the 10 numbers found in array into the sortedArray.
            //Find the way to sort by yourself Just like you would do sorting numbers balls in a basket.

            // sorteren
            // HELP: ik wou de originele array kopiëren zodat deze bewaard bleef, maar als ik de originele afdruk is deze ook gesorteerd
            sortedArray = array; 
            int i, j;
            int temp;


            for (i = 0; i < (array.Length - 1); i++)
            {
                for (j = i + 1; j < array.Length; j++)
                {
                    if (sortedArray[j] <= sortedArray[i])
                    {

                        temp = sortedArray[i];
                        sortedArray[i] = sortedArray[j];
                        sortedArray[j] = temp;
                    }
                }
            }

            Console.WriteLine("originele array");
            PrintArray(array);
            Console.WriteLine();
            Console.WriteLine("gesorteerde array");
            PrintArray(sortedArray);
            Console.WriteLine();

            // neerschrijven arrays  
            return sortedArray;
        }
        static void PrintArray(int[] array)
        {
            for (int k = 0; k < array.Length; k++)
            {
                Console.Write(array[k] + " ");
            }
        }
    }

    class Program_Vic
    {
        public static void run(string[] args)
        {
            int[] nummers = new int[10] { 5, 7, 3, 5, 8, 2, 5, 3, 7, 5 };
            int tempGetal = nummers[0];
            int laatsteGetal = 9;
            while (laatsteGetal > 0)
            {
                for (int teller = 0; teller < laatsteGetal; teller++)
                {
                    if (nummers[teller] > nummers[teller + 1])
                        tempGetal = nummers[teller];
                    nummers[teller] = nummers[teller + 1];
                    nummers[teller + 1] = tempGetal;
                }
                laatsteGetal--;
            }
            for (int teller = 0; teller < nummers.Length - 1; teller++)
            {
                Console.WriteLine(nummers[teller]);
            }
        }
    }

    class Program_Joren
    {
        public static void run(string[] args)
        {
            int[] array = new int[10] { 5, 7, 3, 5, 8, 2, 5, 3, 7, 5 };
            int[] sortedArray = Sort(array);
            PrintArray(array);
            PrintArray(sortedArray);
            Console.Read();
        }

        static int[] Sort(int[] array)
        {
            // Bubble sort
            int bufferSpace = new int();
            int[] sortedArray = new int[10];
            Array.Copy(array, sortedArray, array.Length);

            for (int i = 1; i <= sortedArray.Length; i++)
            {
                for (int j = 0; j < sortedArray.Length - i; j++)
                {
                    if (sortedArray[j] > sortedArray[j + 1])
                    {
                        bufferSpace = sortedArray[j];
                        sortedArray[j] = sortedArray[j + 1];
                        sortedArray[j + 1] = bufferSpace;
                    }
                }
            }
            return sortedArray;
        }

        static void PrintArray(int[] array)
        {
            Console.Write("[ ");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]}");
                Console.Write($"{((i + 1 != array.Length) ? ", " : " ]\n")}");
            }
        }
    }

    class Program_Koen
    {
        public static void run(string[] args) //Deze argumenten kunnen meegegeven worden in CMD :O (mind-blown)
        {
            //Eentje zelf proberen programmeren, zonder binary search
            //Genereer Arr
            Console.WriteLine("Hoeveel getallen wil je genereren");
            int values = 10;
            int.TryParse(Console.ReadLine(), out values);
            int[] array = new int[values];

            FillArray(ref array);
            PrintArray(ref array);

            //Sorteer Arr
            bool swapped = true;
            while (swapped)
            {
                swapped = false;
                for (int i = 0; i < (array.Length - 1); ++i)
                {
                    int numberToSwap = 0;
                    if (array[i] > array[i + 1])
                    {
                        numberToSwap = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = numberToSwap;
                        swapped = true;
                    }
                }
            }
            PrintArray(ref array, ConsoleColor.White);
        }

        static void FillArray(ref int[] arrRef)
        {
            Random r = new Random();

            for (int i = 0; i < arrRef.Length; ++i)
            {
                arrRef[i] = r.Next(0, 100);
            }
        }

        static void PrintArray(ref int[] arrRef, ConsoleColor color = ConsoleColor.Yellow)
        {
            foreach (int value in arrRef)
            {
                Console.ForegroundColor = color;
                Console.WriteLine(value);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }

}
