﻿using System;

namespace Ex.ZonenIfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program_Abdul.run(args);
            //Program_Joren.run(args);
            Program_Hilde.run(args);
        }
    }

    class Program_Joren
    {
        public static void run(string[] args)
        {
            // Vraag aan de gebruiker hoeveel zonen hij/zij heeft.
            // Je kan ook gewoon -1 veranderen naar een getal >= 0 om de input te skippen.
            int aantalZonen = -1;
            while (aantalZonen < 0)
            {
                Console.Clear();
                Console.Write("Hoeveel zonen heb je? ");
                if (!Int32.TryParse(Console.ReadLine(), out aantalZonen))
                {
                    // Antwoord is geen getal -> vraag opnieuw
                    aantalZonen = -1;
                }
            }

            // Geef grammaticaal juist weer en hou het console window open.
            Console.WriteLine($"Je hebt {(aantalZonen > 0 ? aantalZonen.ToString() : "geen")} {(aantalZonen == 1 ? "zoon" : "zoons")}.");
            Console.Read();
        }
    }

    class Program_Thomas
    {
        public static void run(string[] args)
        {
            Console.Write("Hoeveel zonen heb je? ");
            int aantalZonen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Ik heb {(aantalZonen > 1 ? aantalZonen + "zonen" : (aantalZonen <= 0) ? "geen zonen" : aantalZonen + "zoon")}");
        }
    }

    class Program_Scott
    {
        public static void run(string[] args)
        {
            int aantalZonen = 0;

            if (aantalZonen == 0)
            {
                Console.WriteLine("Ik heb geen zonen"); //punt comma vergeten
            }
            else
            {
                Console.WriteLine($"ik heb {aantalZonen} {(aantalZonen > 1 ? "zonen" : "zoon")}");
            }
        }
    }

    class Program_Stijn
    {
        public static void run(string[] args)
        {
            int aantalZonen = 2;

            //Mijn eigen oplossing
            string resultaat = aantalZonen > 1 ? aantalZonen + " zonen" : aantalZonen == 0 ? "geen zonen" : aantalZonen == 1 ? "1 zoon" : "";

            //Mijn 2de oplossing, maar aangepast door feedback tijdens de les
            string resultaat2 = aantalZonen == 0 ? "geen zonen" : aantalZonen > 1 ? aantalZonen + " zonen" : "1 zoon";

            Console.WriteLine($"Ik heb {resultaat2}");
        }
    }

    class Program_Hilde
    {
        public static void run(string[] args)
        {
            int aantalZoons = 3;
            if (aantalZoons == 0)
            {
                Console.WriteLine("ik heb geen zonen");
            }
            else
            {
                Console.WriteLine($"ik heb {aantalZoons} {(aantalZoons > 1 ? "zonen" : "zoon")}");
            }

            // waarschijnlijk niet de mooiste oplossing, maar het werkt 

        }
    }

    class Program_Abdul
    {
        static public void run(string[] args)
        {
            int aantalZoons = 3;
            string output = aantalZoons != 0 ? (aantalZoons > 1 ? $"Ik heb {aantalZoons} zonen" : $"Ik heb één zoon") : "Ik heb geen zonen";

            Console.WriteLine(output);
        }
    }

    class Program_Kris
    {
        public static void run(string[] args)
        {
            int aantalZonen;

            Console.Write("Hoeveel zonen heb je? ");
            aantalZonen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(aantalZonen == 0 ? "Proficiat, u heeft geen zonen" : aantalZonen == 1 ? "Proficiat, u heeft " + aantalZonen + " zoon." : "Proficiat, u heeft " + aantalZonen + " zonen.");
        }
    }

    class Program_Patrick
    {
        public static void run(string[] args)
        {
            int aantalZoons = 3;
            string GeenZoons = "geen zoons";
            string MeerdereZoons = "Zoons";
            string EenZoon = "Zoon";

            //if (aantalZoons != 0)
            //{ Console.WriteLine($"ik heb  {aantalZoons} {(aantalZoons == 1 ? " " + EenZoon : " " + MeerdereZoons)}"); }
            //else
            //{ Console.WriteLine("ik heb geen " + GeenZoons); }

            Console.WriteLine($"ik heb {(aantalZoons == 1 ? aantalZoons + " " + EenZoon : (aantalZoons == 0 ? GeenZoons : aantalZoons + " " + MeerdereZoons))}");
        }
    }

    class Program_Vic
    {
        public static void run(string[] args)
        {
            int aantalZonen = 1;
            if (aantalZonen > 1)
                Console.WriteLine("U hebt " + aantalZonen + " zonen.");
            if (aantalZonen == 1)
                Console.WriteLine("U hebt" + aantalZonen + "zoon.");
            if (aantalZonen < 1)
                Console.WriteLine("U hebt geen zoon.");
        }
    }

    class Program_Niels
    {
        public static void run(string[] args)
        {
            int aantalZonen = 0;

            if (aantalZonen != 0)
            {
                Console.WriteLine($"ik heb {aantalZonen} {(aantalZonen > 1 ? "zonen" : "zoon")}");
            }
            else
            {
                Console.WriteLine("ik heb geen zonen");
            }
        }
    }

    class Program_Koen
    {
        public static void run(string[] args)
        {

            //Nog een iets andere manier gevonden met nog meer nesting!
            Console.WriteLine("Hoeveel zonen heb je?");
            int aantalZonen = 0;
            Int32.TryParse(Console.ReadLine(), out aantalZonen);

            if (aantalZonen == 0)
                Console.WriteLine("Je hebt geen zonen");
            else if (!(aantalZonen < 0))
                Console.WriteLine($"Je hebt {aantalZonen} {((aantalZonen > 1) ? "Zonen" : "zoon")}");
            else Console.WriteLine("Je kan geen negatief aantal zonen hebben!");

            string zonenTekst = (aantalZonen < 0) ? "Je kan geen negatief aantal zonen hebben!" :
                (aantalZonen == 0) ? "Je hebt geen zonen" :
                (aantalZonen > 1) ? $"Je hebt {aantalZonen} zonen" :
                "Je hebt 1 zoon";
            Console.WriteLine(zonenTekst);

            string tekstNew = $"Je hebt {((aantalZonen < 0) ? "negatief aantal zonen" : (aantalZonen == 0) ? "geen zonen" : (aantalZonen > 1) ? $"{aantalZonen} zonen" : "1 zoon")}";
            Console.WriteLine(tekstNew);
        }
    }


}

