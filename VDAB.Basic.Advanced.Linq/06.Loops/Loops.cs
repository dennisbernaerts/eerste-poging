﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VDAB.Basic.Advanced
{
    class Loops
    {
        static public void run(string[] args)
        {
            int leeftijd = 4;
            while (leeftijd < 18)
            {
                Console.WriteLine("te jong om dit te bekijken");
            }


            int stappen = 0;
            int totaalAantalStappen = 100000;
            bool voetenKappot = false;
            do
            {
                if (voetenKappot) 
                    break;      // opgeven

                stappen++;

                if (stappen > 50000)
                    continue;   // Direct naar de volgende iteratie
                Console.WriteLine("Lachen ");
                Console.WriteLine("Praten ");
            } while (stappen < totaalAantalStappen);



            char[] naam = new char[] { 'd', 'e', 'n', 'n', 'i', 's' };
            foreach (char letter in naam)
            {
                Console.WriteLine(" " + letter);
            }

            int whileStappen = 0;
            int whileTotaal = 100000;
            while (whileStappen < whileTotaal)
            {
                whileStappen++;
            }

            for (int forStappen = 0; forStappen < 100000; forStappen++)
            {
                //stappen gebeurd automatisch

            }



            int kolomBreedte = 10;
            char key = 'u';
            if (key == 'u')
            {
                kolomBreedte++;
            }
            else
            {
                kolomBreedte--;
            }


        }
    }
}
