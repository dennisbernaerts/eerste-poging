﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace VDAB.Basic.Advanced
{
    public class ConsoleInputOutput
    {
        static public void Formatting()
        {
            //basis
            string town = "Seattle";
            string name = "abdul";
            int age = 30;
            Console.Write("{0} is {1} years old from {2}!\n", name, age, town);

            //alignment
            Console.WriteLine("{0,6}", 123);
            Console.WriteLine("{0,6}", age);
            Console.WriteLine("{0,6}", 1234);
            Console.WriteLine("{0,6}", 12);
            Console.Write("{0,-6}", 123);
            Console.WriteLine("--end");

            DateTime dt = DateTime.Now;

            Console.WriteLine("{0:t}", dt);  // "4:05 PM"                         ShortTime
            Console.WriteLine("{0:d}", dt);  // "3/9/2008"                        ShortDate
            Console.WriteLine("{0:T}", dt);  // "4:05:07 PM"                      LongTime
            Console.WriteLine("{0:D}", dt);  // "Sunday, March 09, 2008"          LongDate
            Console.WriteLine("{0:f}", dt);  // "Sunday, March 09, 2008 4:05 PM"  LongDate+ShortTime
            Console.WriteLine("{0:F}", dt);  // "Sunday, March 09, 2008 4:05:07 PM" FullDateTime
            Console.WriteLine("{0:g}", dt);  // "3/9/2008 4:05 PM"                ShortDate+ShortTime
            Console.WriteLine("{0:G}", dt);  // "3/9/2008 4:05:07 PM"             ShortDate+LongTime
            Console.WriteLine("{0:m}", dt);  // "March 09"                        MonthDay
            Console.WriteLine("{0:y}", dt);  // "March, 2008"                     YearMonth
            Console.WriteLine("{0:r}", dt);  // "Sun, 09 Mar 2008 16:05:07 GMT"   RFC1123
            Console.WriteLine("{0:s}", dt);  // "2008-03-09T16:05:07"             SortableDateTime
            Console.WriteLine("{0:u}", dt);  // "2008-03-09 16:05:07Z"            UniversalSortableDateTime

            Console.WriteLine("{0:y yy yyy yyyy}", dt);  // "8 08 008 2008"   year
            Console.WriteLine("{0:M MM MMM MMMM}", dt);  // "3 03 Mar March"  month
            Console.WriteLine("{0:d dd ddd dddd}", dt);  // "9 09 Sun Sunday" day
            Console.WriteLine("{0:h hh H HH}", dt);      // "4 04 16 16"      hour 12/24
            Console.WriteLine("{0:m mm}", dt);           // "5 05"            minute
            Console.WriteLine("{0:s ss}", dt);           // "7 07"            second
            Console.WriteLine("{0:f ff fff ffff}", dt);  // "1 12 123 1230"   sec.fraction
            Console.WriteLine("{0:F FF FFF FFFF}", dt);  // "1 12 123 123"    without zeroes
            Console.WriteLine("{0:t tt}", dt);           // "P PM"            A.M. or P.M.
            Console.WriteLine("{0:z zz zzz}", dt);       // "-6 -06 -06:00"   time zone

            Console.WriteLine("______________________ currency ____________________");
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("{0:C2}", 123.456);
            //Output: € 123,46
            string line = String.Format("{0:C2}", 123.456);

            // string -> chars[]  : choose a method in the string object
            char[] chars = line.ToCharArray();

            // chars[] -> bytes[] : use TYPE casting
            byte[] bytes = new byte[chars.Length];
            int i = 0;
            foreach (char c in chars)
            {
                bytes[i++] = (byte)c;
            }

            foreach (byte c in bytes)
            {
                Console.WriteLine("0x{0:X2}", c);
            }
            Console.WriteLine("______________________ currency ____________________");

            Console.WriteLine("{0:D6}", -1234);
            //Output: -001234
            Console.WriteLine("{0:E2}", 123);
            //Output: 1,23E+002
            Console.WriteLine("{0:F2}", -123.456);
            //Output: -123,46
            Console.WriteLine("{0:N2}", 1234567.8);
            //Output: 1 234 567,80
            Console.WriteLine("{0:P}", 0.456);
            //Output: 45,60 %
            Console.WriteLine("{0:X}", 254);
            //Output: FE
            Console.WriteLine("{0:0.00}", 1);
            //Output: 1.00
            Console.WriteLine("{0:#.##}", 0.234);
            //Output: .23
            Console.WriteLine("{0:#####}", 12345.67);
            //Output: 12346
            Console.WriteLine("{0:(0#) ### ## ##}", 29342525);
            //Output: (02) 934 25 25
            Console.WriteLine("{0:%##}", 0.234);
            //Output: %23
            Console.WriteLine("{0:##%}", 0.234);
            //Output: %23

            int col1 = 10;
            Console.WriteLine("{0," + col1 + "}", "den");
            Console.WriteLine("____________________________________________________");


            int read;
            read = Console.Read();
            Console.WriteLine("{0} 0x{1:X} ", read, read);

            ConsoleKeyInfo keyInfo;
            keyInfo = Console.ReadKey();
            Console.WriteLine("\n{0} 0x{1:X} {2}", keyInfo.KeyChar, keyInfo.Key, keyInfo.Modifiers);
            if (Console.KeyAvailable) Console.WriteLine("Console a key is available");

            string number = Console.ReadLine();
            int a = int.Parse(number);
            Console.WriteLine("the number typed = {0}", number);

            number = Console.ReadLine();
            int b;
            bool succes = int.TryParse(number, out b);
            Console.WriteLine("the number typed = {0} {1}", b, succes);

            keyInfo = Console.ReadKey();
            Console.WriteLine("\n{0} 0x{1:X} {2}", keyInfo.KeyChar, keyInfo.Key, keyInfo.Modifiers);
            if (Console.KeyAvailable) Console.WriteLine("Console a key is available");


            Console.WriteLine("____________________________________________________");


            char[] letters = new char[] { 'a', 'b', 'c', 'd' };

            byte[] Bytes = new byte[4];

            string strLetters = "abcd";

            strLetters.ToCharArray();


            int cs = 15;
            Console.WriteLine("{0:"+cs+"}","artikel");

        }
    }
}
