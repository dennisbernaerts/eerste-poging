﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VDAB.Basic.Advanced
{
    public static class Operators
    {
        static void run()
        {

            int getal1 = 1;
            int getal2 = 2;
            int getal3 = getal1 + getal2;  //3

            int getal4 = getal3++;         //3
            int getal5 = ++getal3;

            Console.WriteLine(getal4 + " " + getal5);

            string naam = "vic";

            if ((naam == "vic") || (naam == "niels"))
            {
            }

            int leeftijd = 16;
            bool Toegelaten = (leeftijd < 18) ? false : true;


            int? getal = 5;
            Console.WriteLine("" + (getal ?? -1) + "        " + getal);

            Console.WriteLine("--------------------");
            getal = null;
            Console.WriteLine("" + (getal ?? -1) + "      " + getal);
            Console.WriteLine("--------------------");

            Console.WriteLine((getal1 < getal2 ? "getal1<" : "getal2>="));

            getal = 10;
            Console.WriteLine("dit is " + getal);

            Console.WriteLine(getal + "is een getal");

            Console.WriteLine(getal);


            double myDouble = 5.1d;
            Console.WriteLine((long)myDouble); // 5.1

            myDouble = 5.9d;
            Console.WriteLine((long)myDouble); // 5.1



        }


    }
}
