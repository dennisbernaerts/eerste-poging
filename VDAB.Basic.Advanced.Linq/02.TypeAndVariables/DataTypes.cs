﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VDAB.Basic.Advanced
{
    static class TypeAndVariables
    {

        static public void ex()
        {
            unsafe
            {
                int i;
                bool b;
                long l;

                int* pointerInt = &i;
                bool* pointerBool = &b;
                long* pointerLong = &l;

            }
        }

        static void PrintIntoHexAndBin()
        {
            int getal255 = 255;
            int getal256 = 256;
            int getal257 = 257;

            foreach (int getal in new int[] { getal255, getal256, getal257 })
            {
                Console.WriteLine("int 255 type cast to byte is : {0} \tHexadecimaal: {0:X2} \tBinary: {1}", (byte)getal, ToBinary((byte)getal));
            }

            Console.ReadKey();
        }

        static string ToBinary(byte getal)
        {
            return Convert.ToString(getal, 2).PadLeft(32, '0');
        }
    }
}
