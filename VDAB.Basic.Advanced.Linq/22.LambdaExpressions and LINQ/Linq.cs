﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VDAB.Basic.Advanced
{
    static class LINQ
    {
        //Language Integrated Query
        static public void Summary()
        {
            //everything that implements IEnumerable or IEnumerable<T>
            //same way to query different datasources.xml, sql, monoDB, ...
            //Introduction
            //see The icon of an extentions.
            // predicate : the part of a sentence or clause containing a verb and stating something about the subject.

            var users = new List<string>() { "dennis", "wendy" };

            var userQuery = from user in users select user;

            var userQuery1 = from user in users
                             where user.Contains("m")
                             orderby user.Length ascending
                             select user;

            var userQuery2 = from user in users
                             group user by user.Length into userGroup
                             select userGroup;

            foreach (var userGroup in userQuery2)
            {
                Console.WriteLine("{0} characters long", userGroup.Key);
                foreach(var user in userGroup)
                {
                    Console.WriteLine(user);
                }
            }

            //dir /OS

        }
        
        public static void ShowLargeFilesWithoutLinq(string path)
        {
        }

        // see how keywords are mapped from SQL to Methods. This happens behind the scene.
        public static void ShowLargeFilesWithLinq(string path)
        {
            //SQL syntax. Structured Query Language.
            // group and join.
            // preference is for SQL people.
            //deferred execution. Can be tricky... whatch out

            Console.WriteLine($"SQL way ...");
            var query = from fileinfo in new DirectoryInfo(path).GetFiles()
                        orderby fileinfo.Length descending
                        select fileinfo;
            foreach (var file in query)
            {
                Console.WriteLine($"{file.Name,-40} : {file.Length,10:N0}");
            }

            //Method syntax. 
            //Count() does not exist in SQL and many more.
            Console.WriteLine($"Extentions way ...");
            var query1 = new DirectoryInfo(path).GetFiles()
                        .OrderByDescending(f => f.Length) 
                        .Take(5)
                        .Select(e => e);

            foreach (var file in query)
            {
                Console.WriteLine($"{file.Name,-40} : {file.Length,10:N0}");
            }
        }


        // named method, delegate method, lambda expression ( local method )
        public static void LambdaExpression()
        {
            //1 parameter int , 1 return value int. Last parameter is the return value type.
            Func<int, int> square = x => x * x;
            Func<int, int, int> add = (x, y) => x + y;
            Func<int, int, int> substract = (x, y) =>
            {
                return (x + y);
            };

            Action<int> write = x => Console.WriteLine(x);

            Console.WriteLine(square(add(3, 5)));
        }


        //How LINQ queries behave. 3 types of behaviour. Intented to increase performance.
        //as lazy as possible
        // To Kill defered execution use the ToList, ToArray, To..., .
        // Count() also foreces a complete execution.
        //Defered execution are the ones the return IEnumerable<>.
        //OrderByDescending() forces a complete execution but only when necessary. 
        // https://msdn.microsoft.com/en-us/library/mt693095.aspx
        public class Movie
        {
            internal string Title;
            public double Rating {
                get 
                {
                    Console.WriteLine($"taking rating {Rating} of {Title}");
                    return Rating;
                }
                set 
                {
                    Rating = value;
                }
            }
            internal int Year;
        }

        static List<Movie> movies = new List<Movie>
        {
            new Movie {Title = "Kill Bill 1", Rating = 9, Year = 2003},
            new Movie {Title = "The Good The Bad The ugly", Rating = 8.4, Year = 1966},
            new Movie {Title = "The Godfather", Rating = 9, Year = 1972},
            new Movie {Title = "Carlito's way", Rating = 8.4, Year = 1993},
            new Movie {Title = "Twister", Rating = 2, Year = 1996}
        };

        public static void NonDeferred()
        {

            Console.WriteLine("non deferred");
            IEnumerable<Movie> BestMovies = movies.Where(m => m.Rating > 8).ToList();

        }
        //as lazy as possible. Performance is important for big data !
        //Defered Streaming returns element per element.
        public static void DeferredStreamingExecution()
        {
            Console.WriteLine("defered endless loop");
            IEnumerable<int> query = InfiniteRandom().Where(nr => nr > 5).Take(5);
            
            IEnumerator<int> enumerator =  query.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine($"{enumerator.Current}");
            }
        }

        public static IEnumerable<int> InfiniteRandom()
        {
            Random generator = new Random();
            while(true)
            {
                yield return generator.Next(10);
            }
        }

        //TResult means anything
        //

    }

}
