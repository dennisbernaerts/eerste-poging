﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VDAB.Basic.Advanced
{
    static class Methods
    {
        //static string naam = "Manal";

        static public int sum(int x, int y)
        {
            int result = x + y;
            return result;
        }

        static public bool TEST_sum(int x, int y, int result)
        {
            return sum(x, y) == result;
        }


        static public void DrawDot(int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.Write('o');
        }

        static public void DrawLine(int xStart, int yStart, int xEnd, int yEnd)
        {
            //const int Resolutie = 105;

            while (true)
            {
                // schrijf hier het schrijven van de lijn
                System.Threading.Thread.Sleep(500);
            }

        }

        static public int sum(int[] getallen)
        {
            int result = 0;
            foreach (int getal in getallen)
            {
                result += getal;
            }
            return result;
        }


        static public int GenerateRandomGetal(int minvalue, int maxvalue)
        {
            int randomGetal;

            while (true)
            {

                randomGetal = new Random().Next(9);
                Console.WriteLine(randomGetal);

                Console.ReadKey();
            }

            return randomGetal;
        }

        static public void Snippets()
        {
            int[] oefeningenTeDoen = new int[] { 1 - 2, 9 - 10 };
            foreach (int oefening in Enumerable.Range(1, 7))
            {
                MaakOefening(oefening);
            }

        }

        static public void MaakOefening(int oef)
        {
        }

    }
}
