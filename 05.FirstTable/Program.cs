﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05.FirstTable
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program_Thomas.run(args);
            //Program_Abdul.run(args);
            //Program_Niels.run(args);
            //Program_Hilde.run(args);
            //Program_Vic.run(args);
            Program_Thomas.run(args);

            Console.ReadKey();
        }
    }

    class Program_Manal
    {
        class Program
        {
            private static bool n;
            static void Main(string[] args)
            {
                //maak de tabel
                //Console.Write("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}{5,-25}", "categorie", "artikel", "merk", "prijs", "stock", "nu besteld geleverd op");
                //Console.Write("{0,-16}{1,-15}{2,-15}{3,-15}{4,-15}{5,-25}", "\nmeubels", "stoel", "Ikea", "100", "TRUE", "Sunday 25 APril");
                //Console.Write("{0,-16}{1,-15}{2,-15}{3,-15}{4,-15}{5,-25}", "\nelectronica", "tv", "Samsung", "500", "FALSE", "Sunday 25 APril");
                //Console.Write("{0,-16}{1,-15}{2,-15}{3,-15}{4,-15}{5,-25}", "\nelectronica", "watch", "Garmin", "200", "TRUE", "Sunday 25 APril");
                //maak de kolommen variabel
                int[] i = { -15, -15, -15, -15, -15, -25 };
                string[] firstRow = { "categorie", "artikel", "merk", "prijs", "stock", "nu besteld geleverd op" };
                string[] categorie = { "meubels", "electronica" };
                string[] artikel = { "stoel", "tv", "watch" };
                string[] merk = { "Ikea", "Samsung", "Garmin" };
                float[] prijs = { 100.0f, 500.0f, 200.0f };
                bool[] stock = { true, false, true };
                string levering = "Sunday 25 April";
                Console.WriteLine("{0," + i[0] + "}{1," + i[1] + "}{2," + i[2] + "}{3," + i[3] + "}{4," + i[4] + "}{5," + i[5] + "}", firstRow[0], firstRow[1], firstRow[2], firstRow[3], firstRow[4], firstRow[5]);
                Console.WriteLine("{0," + i[0] + "}{1," + i[1] + "}{2," + i[2] + "}{3," + i[3] + "}{4," + i[4] + "}{5," + i[5] + "}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], levering);
                Console.WriteLine("{0," + i[0] + "}{1," + i[1] + "}{2," + i[2] + "}{3," + i[3] + "}{4," + i[4] + "}{5," + i[5] + "}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], levering);
                Console.WriteLine("{0," + i[0] + "}{1," + i[1] + "}{2," + i[2] + "}{3," + i[3] + "}{4," + i[4] + "}{5," + i[5] + "}", categorie[1], artikel[2], merk[2], prijs[2], stock[2], levering);
                //Console.SetCursorPosition(x,y);    TIP voor deel 2  


                Console.ReadKey();
            }
        }

    }
    public class Program_Niels
    {
        public static void run(string[] args)
        {
            int[] kb = { -16, -10, -12, -8, -10, -25 };     //kolombreedte negatief = links uitlijnen
            //int[] kb = { 16, 10, 12, 8, 10, 25 };         //positief = rechts uitlijnen
            string dateFormat = ":D";                       //makkelijk om de manier van datum weergeven aan te passen naar bvb string dateFormat = ":d";
            string[] header = { "Categorie", "Artikel", "Merk", "Prijs", "Stock", "Nu besteld, geleverd op" };
            string[] categorie = { "Meubels", "Elektronica", "Elektronica" };
            string[] artikel = { "Stoel", "TV", "Watch" };
            string[] merk = { "Ikea", "Samsung", "Garmin" };
            string[] prijs = { "100", "500", "200" };
            bool[] stock = { true, false, true };
            int[] breedte = { 16, 10, 12, 8, 10, 25 };      //enkel gebruikt voor PadLeft/PadRight tests

            //andere mogelijkheid: .PadLeft() en .PadRight() gebruiken
            //nadeel: als je wil veranderen van uitlijning links/rechts is het meer werk dan -+ veranderen in de array
            //PadLeft = positieve waarden = rechts uitlijnen
            //Console.WriteLine(header[0].PadLeft(breedte[0]) + header[1].PadLeft(breedte[1]) + header[2].PadLeft(breedte[2]) + header[3].PadLeft(breedte[3]) + header[4].PadLeft(breedte[4]) + header[5].PadLeft(breedte[5]));
            //PadRight = negatieve waarden = links uitlijnen
            //Console.WriteLine(header[0].PadRight(breedte[0]) + header[1].PadRight(breedte[1]) + header[2].PadRight(breedte[2]) + header[3].PadRight(breedte[3]) + header[4].PadRight(breedte[4]) + header[5].PadRight(breedte[5]));


            DateTime dateOrdered = DateTime.Now;
            int daysToDeliver = 2;          //levertijd 2 dagen
            DateTime[] leverTijd = { dateOrdered.AddDays(daysToDeliver), dateOrdered.AddDays(daysToDeliver), dateOrdered.AddDays(daysToDeliver) };

            Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + "}", header[0], header[1], header[2], header[3], header[4], header[5]);
            Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], leverTijd[0]);
            Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], leverTijd[1]);
            Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], leverTijd[2]);

            Console.WriteLine("Press 'u' to widen the columns or use left/right arrows");

            ConsoleKey inputKey = Console.ReadKey().Key;

            do
            {
                if (inputKey == ConsoleKey.U)
                {
                    for (int i = 0; i < kb.Length; i++)
                    {
                        kb[i]--;
                    }
                    Console.Clear(); //zet cursor automatisch op (0, 0)
                    //Console.SetCursorPosition(0, 0);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + "}", header[0], header[1], header[2], header[3], header[4], header[5]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], leverTijd[0]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], leverTijd[1]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], leverTijd[2]);

                    //Console.WriteLine(""+ kb[0] + kb[1] + kb[2] + kb[3] + kb[4] + kb[5]);

                    Console.WriteLine("Press 'u' to widen the columns or use left/right arrows");
                    inputKey = Console.ReadKey().Key;


                }

                else if (inputKey == ConsoleKey.LeftArrow)
                {
                    for (int i = 0; i < kb.Length; i++)
                    {
                        kb[i]++;
                    }
                    Console.Clear(); //zet cursor automatisch op (0, 0)
                    //Console.SetCursorPosition(0, 0);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + "}", header[0], header[1], header[2], header[3], header[4], header[5]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], leverTijd[0]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], leverTijd[1]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], leverTijd[2]);

                    //Console.WriteLine("" + kb[0] + kb[1] + kb[2] + kb[3] + kb[4] + kb[5]);

                    Console.WriteLine("Press 'u' to widen the columns or use left/right arrows");
                    inputKey = Console.ReadKey().Key;
                }

                else if (inputKey == ConsoleKey.RightArrow)
                {
                    for (int i = 0; i < kb.Length; i++)
                    {
                        kb[i]--;
                    }
                    Console.Clear(); //zet cursor automatisch op (0, 0)
                    //Console.SetCursorPosition(0, 0);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + "}", header[0], header[1], header[2], header[3], header[4], header[5]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], leverTijd[0]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], leverTijd[1]);
                    Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + dateFormat + "}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], leverTijd[2]);

                    //Console.WriteLine("" + kb[0] + kb[1] + kb[2] + kb[3] + kb[4] + kb[5]);

                    Console.WriteLine("Press 'u' to widen the columns or use left/right arrows");
                    inputKey = Console.ReadKey().Key;
                }
            } while ((inputKey == ConsoleKey.RightArrow) || (inputKey == ConsoleKey.LeftArrow) || (inputKey == ConsoleKey.U));





            #region DateTime tests
            //DateTime[] leverTijd = { DateTime.Today, DateTime.Today, DateTime.Now };      //eerst geprobeerd om DateTime.Today+2 te gebruiken
            //Console.WriteLine(DateTime.Today);                                            //Today en Now geven zelfde output als je {5,-25:D} gebruikt
            //Console.WriteLine(DateTime.Now);
            //Console.WriteLine("{0:D}",DateTime.Today);
            //Console.WriteLine("{0:D}",DateTime.Now);
            #endregion

            #region variable voor type datum weergave gebruiken
            // :D nog vervangen door een variabele

            //Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + "}", header[0], header[1], header[2], header[3], header[4], header[5]);
            //Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + ":D}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], leverTijd[0]);
            //Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + ":D}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], leverTijd[1]);
            //Console.WriteLine("{0," + kb[0] + "}{1," + kb[1] + "}{2," + kb[2] + "}{3," + kb[3] + "}{4," + kb[4] + "}{5," + kb[5] + ":D}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], leverTijd[2]);
            #endregion

            #region history
            ////bedankt vic :)

            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25:D}", header[0], header[1], header[2], header[3], header[4], header[5]);
            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25:D}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], leverTijd[0]);
            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25:D}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], leverTijd[1]);


            ////hoe ik gestart was

            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25}", "Categorie", "Artikel", "Merk", "Prijs", "stock","Nu besteld, geleverd op");
            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25}", "meubels", "stoel", "ikea", "100", "TRUE", "Sunday 25 april");
            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25}", "elektronica", "tv", "samsung", "500", "FALSE", "Sunday 25 april");
            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25}", "elektronica", "watch", "garmin", "200", "TRUE", "Sunday 25 april");

            //Console.WriteLine("{0,-16}{1,-12}{2,-12}{3,-12}{4,-12}{5,-25:D}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], leverTijd[2]);
            #endregion




            // pijltje naar rechts kolommen groter maken
            // pijltje naar links kolommen kleiner maken
            // nog te doen: aparte methodes maken voor PrintTabel en ReadInputKey/CompareInputKey

        }

    }

    class Program_Vic
    {
           public static void run(string[] args)
            {
                Console.WriteLine("Hoe breed wens je de kolon?");
                int breedte = int.Parse(Console.ReadLine());


                string[] headers = { "categorie", "artikel", "merk", "prijs", "stock", "nu besteld geleverd op".PadRight(breedte) };
                string[] categorie = { "meubels", "electronica", "electronica".PadRight(breedte) };
                string[] artikel = { "stoel", "tv", "watch".PadRight(breedte) };
                string[] merk = { "ikea", "samsung", "gamin".PadRight(breedte) };
                string[] prijs = { "100", "500", "200" };
                string[] stock = { Convert.ToString(true), Convert.ToString(false), Convert.ToString(true) };
                string[] nuBesteld = { Convert.ToString(DateTime.Parse("Sunday 26 April")), Convert.ToString(DateTime.Parse("Sunday 26 April")), Convert.ToString(DateTime.Parse("Sunday 26 April")) };


                Console.WriteLine(headers[0].PadRight(breedte) + headers[1].PadRight(breedte) + headers[2].PadRight(breedte) + headers[3].PadRight(breedte) + headers[4].PadRight(breedte) + headers[5].PadRight(breedte));
                Console.WriteLine(categorie[0].PadRight(breedte) + artikel[0].PadRight(breedte) + merk[0].PadRight(breedte) + prijs[0].PadRight(breedte) + stock[0].PadRight(breedte) + nuBesteld[0].PadRight(breedte));
                Console.WriteLine(categorie[1].PadRight(breedte) + artikel[1].PadRight(breedte) + merk[1].PadRight(breedte) + prijs[1].PadRight(breedte) + stock[1].PadRight(breedte) + nuBesteld[1].PadRight(breedte));
                Console.WriteLine(categorie[2].PadRight(breedte) + artikel[2].PadRight(breedte) + merk[2].PadRight(breedte) + prijs[2].PadRight(breedte) + stock[2].PadRight(breedte) + nuBesteld[2].PadRight(breedte));


                Console.WriteLine("Druk op u om de kolonnen breder te maken, of s om te stoppen.");
                char brederMaker = 'a';
                brederMaker = Convert.ToChar(Console.Read());
                while (brederMaker != 's')
                {
                    if (brederMaker == 'u')
                    {
                        breedte++;
                        Console.WriteLine(headers[0].PadRight(breedte) + headers[1].PadRight(breedte) + headers[2].PadRight(breedte) + headers[3].PadRight(breedte) + headers[4].PadRight(breedte) + headers[5].PadRight(breedte));
                        Console.WriteLine(categorie[0].PadRight(breedte) + artikel[0].PadRight(breedte) + merk[0].PadRight(breedte) + prijs[0].PadRight(breedte) + stock[0].PadRight(breedte) + nuBesteld[0].PadRight(breedte));
                        Console.WriteLine(categorie[1].PadRight(breedte) + artikel[1].PadRight(breedte) + merk[1].PadRight(breedte) + prijs[1].PadRight(breedte) + stock[1].PadRight(breedte) + nuBesteld[1].PadRight(breedte));
                        Console.WriteLine(categorie[2].PadRight(breedte) + artikel[2].PadRight(breedte) + merk[2].PadRight(breedte) + prijs[2].PadRight(breedte) + stock[2].PadRight(breedte) + nuBesteld[2].PadRight(breedte));
                    }
                    brederMaker = Convert.ToChar(Console.Read());
                }


                Console.ReadKey();
            }

    }
    class Program_Thomas
    {
        public static void run(string[] args)
        {
            bool juist = true;
            bool fout = false;
            string[] header = new string[6] { "Categorie", "Artikel", "Merk", "Prijs", "Stock", "nu besteld geleverd op" };
            string[] categories = new string[] { "meubels", "elektronica" };
            string[] artikels = new string[] { "stoel", "tv", "watch" };
            string[] merken = new string[] { "ikea", "samsung", "garmin" };
            int[] prijzen = new int[] { 100, 500, 200 };

            #region leverings datum
            //Toevoegen leverings datum
            DateTime dt = DateTime.Now;
            dt = dt.AddDays(5);
            //Als levering op zondag valt + 1 dag!
            if ($"{dt:dddd}" == "Sunday")
            {
                dt = dt.AddDays(1);
            }
            #endregion

            #region Test variabele lengte met For loop
            //Test variabele lengte met For loop:
            /*for (int i = 0; i < header.Length; i++)
            {
                int lengte = (header[i].Length + 6) * -1;
                Console.Write("{0, " + lengte + "}", header[i]);

            }*/
            #endregion

            #region zonder variabele lengte
            //zonder variabele lengte
            /*Console.WriteLine("{0, -16}{1, -12}{2, -10}{3, -10}{4, -11}{5, -20}", 
                header[0], header[1], header[2], header[3], header[4], header[5]);*/
            #endregion

            #region "BruteForce"
            //Test BruteForce:
            int lengteC = (header[0].Length + 6) * -1;
            int lengteA = (header[1].Length + 6) * -1;
            int lengteM = (header[2].Length + 6) * -1;
            int lengteP = (header[3].Length + 6) * -1;
            int lengteS = (header[4].Length + 6) * -1;
            int lengteB = (header[5].Length + 6) * -1;
            //Met BruteForce manier:
            Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                header[0], header[1], header[2], header[3], header[4], header[5]);
            Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                categories[0], artikels[0], merken[0], prijzen[0], juist, $"{dt:dddd dd MMMM}");
            Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                categories[1], artikels[1], merken[1], prijzen[1], fout, $"{dt:dddd dd MMMM}");
            Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                categories[1], artikels[2], merken[2], prijzen[2], juist, $"{dt:dddd dd MMMM}");
            #endregion

            Console.WriteLine("______________________________________________________________________________________________________");
            Console.WriteLine("Vergroot of verklein tabel met pijltjes, pijl naar boven om te stoppen");

            var pijl = Console.ReadKey().Key;
            while (pijl != ConsoleKey.UpArrow)
            {
                #region rechtse pijl
                //Breedte vergroten met rechtse pijl
                while (pijl == ConsoleKey.RightArrow)
                {
                    Console.SetCursorPosition(0, 0);
                    lengteC--;
                    lengteA--;
                    lengteM--;
                    lengteP--;
                    lengteS--;
                    lengteB--;
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                    header[0], header[1], header[2], header[3], header[4], header[5]);
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                        categories[0], artikels[0], merken[0], prijzen[0], juist, $"{dt:dddd dd MMMM}");
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                        categories[1], artikels[1], merken[1], prijzen[1], fout, $"{dt:dddd dd MMMM}");
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                        categories[1], artikels[2], merken[2], prijzen[2], juist, $"{dt:dddd dd MMMM}");
                    pijl = Console.ReadKey().Key;
                }
                #endregion

                #region linkse pijl
                //Breedte verkleinen met linkse pijl
                while (pijl == ConsoleKey.LeftArrow)
                {
                    Console.SetCursorPosition(0, 0);
                    lengteC++;
                    lengteA++;
                    lengteM++;
                    lengteP++;
                    lengteS++;
                    lengteB++;
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                    header[0], header[1], header[2], header[3], header[4], header[5]);
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                        categories[0], artikels[0], merken[0], prijzen[0], juist, $"{dt:dddd dd MMMM}");
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                        categories[1], artikels[1], merken[1], prijzen[1], fout, $"{dt:dddd dd MMMM}");
                    Console.WriteLine("{0, " + lengteC + "}{1, " + lengteA + "}{2, " + lengteM + "}{3, " + lengteP + "}{4, " + lengteS + "}{5, " + lengteB + "}",
                        categories[1], artikels[2], merken[2], prijzen[2], juist, $"{dt:dddd dd MMMM}");
                    pijl = Console.ReadKey().Key;
                }
                #endregion
            }
        }

    }

    class Program_Koen
    {
        private struct TableContent
        {
            public TableContent(string categorie, string artikel, string merk, int prijs, bool inStock, DateTime geleverdOp)
            {
                Categorie = categorie;
                Artikel = artikel;
                Merk = merk;
                Prijs = prijs;
                InStock = inStock;
                GeleverdOp = geleverdOp;
            }

            public string Categorie { get; }
            public string Artikel { get; }
            public string Merk { get; }
            public int Prijs { get; }
            public bool InStock { get; }
            public DateTime GeleverdOp { get; }
        }

        const int Rows = 6;

        static void TableExercise()
        {
            //create table
            Console.Write("Hoe groot moeten de kolommen zijn? ");
            int formatSpacing;
            Int32.TryParse(Console.ReadLine(), out formatSpacing);

            //Table contents
            TableContent[] products = new TableContent[3];

            DateTime newDate = new DateTime(2020, 4, 25);
            products[0] = new TableContent("Meubels", "Stoel", "Ikea", 100, true, newDate);
            products[1] = new TableContent("Electronica", "Tv", "Samsung", 500, false, newDate);
            products[2] = new TableContent("Electronica", "Watch", "Garmin", 200, true, newDate);

            CreateTable(formatSpacing, products);

            //Program loop
            ConsoleKey key = ConsoleKey.Applications;

            while (key != ConsoleKey.Q)
            {
                Console.Write("Q om af te sluiten, rechts op groter te maken, links om te verkleinen. ");
                key = Console.ReadKey().Key;
                Console.WriteLine("");

                if (key == ConsoleKey.LeftArrow)
                {
                    --formatSpacing;
                    if (formatSpacing < -50 || formatSpacing > 50)
                        formatSpacing = -50;

                    CreateTable(formatSpacing, products);
                }

                if (key == ConsoleKey.RightArrow)
                {
                    ++formatSpacing;
                    if (formatSpacing < -50 || formatSpacing > 50)
                        formatSpacing = 50;

                    CreateTable(formatSpacing, products);
                }
            }

            Console.WriteLine("\nBye!");
        }

        static void CreateTable(int formatSpacing, TableContent[] products)
        {
            string divider = ""; //5 rows + begin en einde
            int amountNeeded = (Math.Abs(formatSpacing) * Rows) + (Rows + 1);
            for (int i = 0; i < amountNeeded; i++)
            {
                divider += "-";
            }

            Console.WriteLine(divider);
            Console.WriteLine(String.Format($"|{{0,{formatSpacing}}}|{{1,{formatSpacing}}}|{{2,{formatSpacing}}}|{{3,{formatSpacing}}}|{{4,{formatSpacing}}}|{{5,{formatSpacing}: dd MM yyyy}}|", "Categorie", "Artikel", "Merk", "Prijs", "Stock", "Nu besteld geleverd op"));
            Console.WriteLine(divider);

            foreach (TableContent tableInfo in products)
            {
                Console.WriteLine(string.Format($"|{{0,{formatSpacing}}}|{{1,{formatSpacing}}}|{{2,{formatSpacing}}}|{{3,{formatSpacing}}}|{{4,{formatSpacing}}}|{{5,{formatSpacing}: dd MM yyyy}}|", tableInfo.Categorie, tableInfo.Artikel, tableInfo.Merk, tableInfo.Prijs, tableInfo.InStock, tableInfo.GeleverdOp));
                Console.WriteLine(divider);
            }
        }

    }

    //class Program_Thomas
    //{
    //    static void run(string[] args)
    //    {
    //        bool juist = true;
    //        bool fout = false;

    //        //Toevoegen leverings datum
    //        DateTime dt = DateTime.Now;
    //        dt = dt.AddDays(5);

    //        //Als levering op zondag valt + 1 dag!
    //        if ($"{dt:dddd}" == "Sunday")
    //        {
    //            dt = dt.AddDays(1);
    //        }

    //        //Test: variabele lengte
    //        string[] header = new string[6] { "Categorie", "Artikel", "Merk", "Prijs", "Stock", "nu besteld geleverd op" };
    //        string[] categories = new string[] { "meubels", "elektronica" };
    //        string[] artikels = new string[] { "stoel", "tv", "watch" };
    //        string[] merken = new string[] { "ikea", "samsung", "garmin" };
    //        int[] prijzen = new int[] { 100, 500, 200 };
    //        for (int i = 0; i < header.Length; i++)
    //        {
    //            int lengte = (header[i].Length + 6) * -1;
    //            Console.Write("{0, " + lengte + "}", header[i]);

    //        }

    //        //zonder variabele lengte
    //        /*Console.WriteLine("{0, -16}{1, -12}{2, -10}{3, -10}{4, -11}{5, -20}", 
    //            header[0], header[1], header[2], header[3], header[4], header[5]);*/
    //        Console.WriteLine("\n{0, -15}{1, -13}{2, -10}{3, -11}{4, -11}{5, -28}",
    //            categories[0], artikels[0], merken[0], prijzen[0], juist, $"{dt:dddd dd MMMM}");
    //        Console.WriteLine("{0, -15}{1, -13}{2, -10}{3, -11}{4, -11}{5, -28}",
    //            categories[1], artikels[1], merken[1], prijzen[1], fout, $"{dt:dddd dd MMMM}");
    //        Console.WriteLine("{0, -15}{1, -13}{2, -10}{3, -11}{4, -11}{5, -28}",
    //            categories[1], artikels[2], merken[2], prijzen[2], juist, $"{dt:dddd dd MMMM}");
    //    }
    //}

    class Program_Stijn
    {
        static void run(string[] args)
        {
            #region  Console Format Oef

            string[] categories = { "Categorie", "meubels", "electronica", "electronica" };
            string[] artikels = { "Artikel", "stoel", "tv", "watch" };
            string[] merken = { "Merk", "ikea", "samsung", "garmin" };
            string colPrijsHeader = "Prijs";
            int[] prijzen = { 100, 500, 200 };
            string colStockHeader = "Stock";
            bool[] inStockStatus = { true, false, true };
            string colLeverDataHeader = "nu besteld geleverd op";
            DateTime[] leverData = { new DateTime(2020, 4, 25), new DateTime(2020, 4, 25), new DateTime(2020, 4, 25) };
            int aantalRijen = 3;

            int colLength = 25;


            //// Niet variabel
            Console.WriteLine("{0,-15}{1,-12}{2,-12}{3,-12}{4,-12}{5,-12}", categories[0], artikels[0], merken[0], colPrijsHeader, colStockHeader, colLeverDataHeader);
            for (int i = 0; i < aantalRijen; i++)
            {
                Console.WriteLine("{0,-15}{1,-12}{2,-12}{3,-12}{4,-12}{5,-12:dddd dd MMMM}", categories[i + 1], artikels[i + 1], merken[i + 1], prijzen[i], inStockStatus[i].ToString().ToUpper(), leverData[i]);
            }

            Console.WriteLine("------------------------------------------------------------------------------------------------------------------");

            ////  Wel variabel
            ////  Dit is variabel en de spacing word geregeld door de colLength variabele

            Console.WriteLine($"{categories[0].PadRight(colLength)}{artikels[0].PadRight(colLength)}{merken[0].PadRight(colLength)}{colPrijsHeader.PadRight(colLength)}{colStockHeader.PadRight(colLength)}{colLeverDataHeader.PadRight(colLength)}");
            for (int i = 0; i < aantalRijen; i++)
            {
                Console.WriteLine($"{categories[i + 1].PadRight(colLength)}{artikels[i + 1].PadRight(colLength)}{merken[i + 1].PadRight(colLength)}{prijzen[i].ToString().PadRight(colLength)}{inStockStatus[i].ToString().ToUpper().PadRight(colLength)}{leverData[i].ToString("dddd dd MMMM").PadRight(colLength)}");
            }

            #endregion


            Console.ReadKey();
        }
    }

    //class Program_Vic
    //{
    //    public static void run(string[] args)
    //    {
    //        string[] headers = { "categorie", "artikel", "merk", "prijs", "stock", "nu besteld geleverd op" };
    //        string[] categorie = { "meubels", "electronica", "electronica" };
    //        string[] artikel = { "stoel", "tv", "watch" };
    //        string[] merk = { "ikea", "samsung", "gamin" };
    //        int[] prijs = { 100, 500, 200 };
    //        bool[] stock = { true, false, true };
    //        DateTime[] nuBesteld = { DateTime.Parse("Sunday 26 April"), DateTime.Parse("Sunday 26 April"), DateTime.Parse("Sunday 26 April") };

    //        Console.WriteLine("{0,-15} {1,-10} {2,-10} {3,-10} {4,-10} {5,-10}", headers[0], headers[1], headers[2], headers[3], headers[4], headers[5]);
    //        Console.WriteLine("{0,-15} {1,-10} {2,-10} {3,-10} {4,-10} {5,-10}", categorie[0], artikel[0], merk[0], prijs[0], stock[0], nuBesteld[0]);
    //        Console.WriteLine("{0,-15} {1,-10} {2,-10} {3,-10} {4,-10} {5,-10}", categorie[1], artikel[1], merk[1], prijs[1], stock[1], nuBesteld[1]);
    //        Console.WriteLine("{0,-15} {1,-10} {2,-10} {3,-10} {4,-10} {5,-10}", categorie[2], artikel[2], merk[2], prijs[2], stock[2], nuBesteld[2]);


    //        Console.ReadKey();
    //    }
    //}
    class Program_Abdul
    {
        public static void run(string[] args)
        {
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine("categorie   | artikel  |   merk   |  prijs  | stock |  nu besteld geleverd op");
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine(String.Format("{0,-11} | {1,-8} | {2,-8} | {3, -7} | {4, -5} | {5,-5}", "meubels", "stoel", "ikea", "100", "TRUE", "Sunday 25 april"));
            Console.WriteLine(String.Format("{0,-11} | {1,-8} | {2,-8} | {3, -7} | {4, -5} | {5,-5}", "electronica", "tv", "samsung", "500", "FALSE", "Sunday 25 april"));
            Console.WriteLine(String.Format("{0,-11} | {1,-8} | {2,-8} | {3, -7} | {4, -5} | {5,-5}", "meubels", "stoel", "ikea", "100", "TRUE", "Sunday 25 april"));
            Console.WriteLine("-------------------------------------------------------------------------");

        }
    }

    class Program_Hilde
    {
        public static void run(string[] args)
        {
            // oefening van Dennis (zie ook lesnotities)
            // maak een tabel met artikels

            Console.WriteLine();
            string[] ar1 = new string[] { "categorie", "meubels", "electronica", "electronica" };
            string[] ar2 = new string[] { "artikel", "stoel", "tv", "watch" };
            string[] ar3 = new string[] { "merk", "ikea", "samsung", "garmin" };
            string[] ar4 = new string[] { "prijs", "100", "500", "200" };
            string[] ar5 = new string[] { "stock", "TRUE", "FALSE", "TRUE" };
            string[] ar6 = new string[] { "nu besteld geleverd op", "Sunday 25 April", "Sunday 25 April", "Sunday 25 April" };

            for (int k = 0; k < ar1.Length; k++)
            {
                Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}{5,-15}", ar1[k], ar2[k], ar3[k], ar4[k], ar5[k], ar6[k]);

            }
        }
    }

}
