﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06.ConditionalStatements
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    class Program_Stijn
    {
        static void oef8(string[] args)
        {
            ///Write a program that, depending on the user’s choice, inputs int, double or string variable.
            ///If the variable is int or double, the program increases it by 1. If the variable is a string, the program appends "*" at the end.
            ///Print the result at the console. Use switch statement.
            ///
            ///Dit was verwarrend verwoord vanuit het boek, men wilt dat je 0, 1 of 2 ingeeft en gebasseerd daarop via een switch de nodige aanpassingen doet
            ///als we gebruik willen maken van ---is--- gaat dit niet met een switch
            ///

            ///Met een switch case zoals verwacht word vanuit het boek
            //Console.Write("Write 0 for int, 1 for double or 2 for string: ");
            //int digit;
            //Int32.TryParse(Console.ReadLine(), out digit);

            //while (digit > 2 || digit < 0)
            //{
            //    Console.WriteLine("Invalid digit, please use a single digit between 0 - 2");
            //    Int32.TryParse(Console.ReadLine(), out digit);
            //}

            //switch (digit)
            //{
            //    case 0:
            //    case 1:
            //        Console.WriteLine(++digit);
            //        break;
            //    case 2:
            //        Console.WriteLine(digit + "*");
            //        break;
            //}

            ///Door gebruikt te maken van is met if checks
            ///
            Console.Write("Write an int, a double (use , for decimal point) or a string: ");
            int i;
            double d;
            string s;

            s = Console.ReadLine();

            if (Int32.TryParse(s, out i))
            {
                Console.WriteLine("Input is an integer");
            }
            else if (Double.TryParse(s, out d))
            {
                Console.WriteLine("Input is a double");
            }
            else
            {
                Console.WriteLine("Input is a string");
            }

        }
    }
}
