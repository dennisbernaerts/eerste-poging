﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsAndExpresions
{
    class Program
    {
        //static void Main(string[] args)
        //{

        //}
    }

    class Program_Vic
    {

        static void oef8(string[] args)
        {
            int x = Convert.ToInt32(Console.ReadLine());
            int y = Convert.ToInt32(Console.ReadLine());
            if (x < -5 || x > 5 || y < -5 || y > 5)
            {
                Console.WriteLine("Het gegeven punt ligt buiten de cirkel");
            }
            else
            {
                Console.WriteLine("Het gegeven punt ligt in de cirkel");
            }
            Console.ReadKey();
        }

        static void oef7(string[] args)
        {
            Console.WriteLine("Hoeveel weegt de astronaut in kilogram?");
            float AardsGewicht = Convert.ToInt32(Console.ReadLine());
            float gewichtManOpMaan = (AardsGewicht / 100) * 17;
            Console.WriteLine("Onze astronaut van {0} kg weegt op de maan slechts {1} kg.", AardsGewicht, gewichtManOpMaan);
            Console.ReadKey();
        }

        static void oef6(string[] args)
        {
            int lengte = Convert.ToInt32(Console.ReadLine());
            int breedte = Convert.ToInt32(Console.ReadLine());
            int omtrek = lengte * 2 + breedte * 2;
            int oppervlakte = lengte * breedte;
            Console.WriteLine("De omtrek is gelijk aan {0}, en de oppervlakte is gelijk aan {1}", omtrek, oppervlakte);

            Console.ReadKey();
        }

        static void oef3(string[] args)
        {
            int getalUser = Convert.ToInt32(Console.ReadLine());
            int gedeeld = getalUser / 100;
            int laatsteGetal = gedeeld % 10;
            if (laatsteGetal == 7)
            {
                Console.WriteLine("{0} heeft als derde getal een 7!", getalUser);
            }
            else
            {
                Console.WriteLine("{0} heeft niet 7 als derde getal.", getalUser, laatsteGetal);
            }
            Console.ReadKey();
        }

        static void oef2(string[] args)
        {
            int getalUser = Convert.ToInt32(Console.ReadLine());
            bool isDeelbaar = false;
            if (getalUser % 5 == 0 && getalUser % 7 == 0)
            {
                isDeelbaar = true;
                Console.WriteLine("Het ingegeven getal is deelbaar door zowel 5 als 7", isDeelbaar);
            }
            else
            {
                Console.WriteLine("Het ingegeven getal is niet deelbaar door ofwel 5 of 7", isDeelbaar);
            }
            Console.ReadKey();
        }

        static void oef1(string[] args)
        {
            int oefeningEvenOneven = Convert.ToInt32(Console.ReadLine());
            bool evenGetal = false;
            if (oefeningEvenOneven % 2 == 0)
            {
                evenGetal = true;
                Console.WriteLine("Het betreft een even getal");
            }
            else
            {
                Console.WriteLine("Het betreft een oneven getal");
            }
            Console.ReadKey();
        }

        static void oef14(string[] args)
        {
            bool priemGetal = true;
            int getal = Convert.ToInt32(Console.ReadLine());
            while (getal < 1 || getal > 100)
            {
                getal = Convert.ToInt32(Console.ReadLine());
            }
            for (int teller = 2; teller < getal; teller++)
            {
                if (getal % teller == 0)
                {
                    Console.WriteLine("Getal is geen priemgetal");
                    priemGetal = false;
                    break;
                }
            }
            if (priemGetal == true)
            {
                Console.WriteLine("Getal is een priemgetal");
            }
            Console.ReadKey();
        }

        static void oef10(string[] args)
        {
            int getalUser = Convert.ToInt32(Console.ReadLine());
            int a = getalUser % 10;
            int b = (getalUser / 10) % 10;
            int c = (getalUser / 100) % 10;
            int d = (getalUser / 1000) % 10;
            int som = a + b + c + d;
            Console.WriteLine("De som is gelijk aan {0}", som);
            Console.WriteLine("{0} {1} {2} {3}", d, c, b, a);
            Console.WriteLine("{0} {1} {2} {3}", d, a, b, c);
            Console.WriteLine("{0} {1} {2} {3}", a, c, b, d);


            Console.ReadKey();
        }

        static void oef9(string[] args)
        {
            int x = Convert.ToInt32(Console.ReadLine());
            int y = Convert.ToInt32(Console.ReadLine());
            if (x < -5 || x > 5 || y < -5 || y > 5)
            {
                Console.WriteLine("Het gegeven punt ligt buiten de cirkel");
            }
            else
            {
                Console.WriteLine("Het gegeven punt ligt in de cirkel");
            }

            if (x < -1 || x > 5 || y < 1 || y > 5)
            {
                Console.WriteLine("Het gegeven punt ligt buiten de rechthoek");
            }
            else
            {
                Console.WriteLine("Het gegeven punt ligt in de rechthoek");
            }

            Console.ReadKey();
        }

    }

    class Program_Stijn
    {
            static void run(string[] args)
            {
                #region Oef 1
                ////Write an expression that checks whether an integer is odd or even.

                //Console.Write("Geef een getal op: ");
                //int getal;
                //Int32.TryParse(Console.ReadLine(), out getal);

                //if (getal%2  == 0)
                //{
                //    Console.WriteLine("Getal is even");
                //}
                //else
                //{
                //    Console.WriteLine("Getal is oneven");
                //}
                #endregion
                #region Oef 2
                ////Write a Boolean expression that checks whether a given integer is divisible by both 5 and 7, without a remainder.

                //Console.Write("Geef een getal op: ");
                //int getal2;
                //Int32.TryParse(Console.ReadLine(), out getal2);

                //bool deelbaarDoor5En7 = ((getal2 % 5 == 0) && (getal2 % 7 == 0));

                //if (deelbaarDoor5En7)
                //{
                //    Console.WriteLine("Getal is deelbaar door 5 en 7");
                //}
                //else
                //{
                //    Console.WriteLine("Getal is niet deelbaar door 5 en 7");
                //}
                #endregion
                #region Oef 3
                ////Write an expression that checks for a given integer if its third digit(right to left) is 7.

                //Console.Write("Geef een getal op: ");
                //int getal2;
                //Int32.TryParse(Console.ReadLine(), out getal2);
                //int getal3 = (getal2 / 1000) % 10;
                //// 158796
                //// 158796 / 100 = 1587.96
                //// 1587 / 10 = 158.7
                //// 1587 % 10 = 7
                //// We gaan gebruik maken van delen door 100 en 10 om ons cijfers op te schuiven zodat we tot aan ons 3de getal komen
                //// Als we bijvoorbeeld het 4de getal willen weten, delen we eerst door 1000 endan % 10

                //if (getal3 == 7)
                //{
                //    Console.WriteLine("3de cijfer in getal is 7");
                //}
                //else
                //{
                //    Console.WriteLine("3de cijfer in getal is niet 7");
                //}
                #endregion
                #region Oef 6
                ////Write a program that prints on the console the perimeter and the area of a rectangle by given side and height entered by the user.

                //Console.Write("Wat is de hoogte: ");
                //double lengte;
                //Double.TryParse(Console.ReadLine(), out lengte);

                //Console.Write("Wat is de breedte: ");
                //double breedte;
                //Double.TryParse(Console.ReadLine(), out breedte);

                //double omtrek = (lengte + breedte) * 2;
                //double oppervlakte = lengte * breedte;
                //Console.WriteLine($"De omtrek van deze rechthoek is: {omtrek}m ");
                //Console.WriteLine($"De oppervlakte van deze rechthoek is: {oppervlakte}m²");

                #endregion
                #region Oef 7

                ////The gravitational field of the Moon is approximately 17% of that on the Earth.
                ////Write a program that calculates the weight of a man on the moon by a given weight on the Earth.
                ///
                //Console.Write("Wat is uw gewicht in kg: ");
                //double gewicht;
                //Double.TryParse(Console.ReadLine(), out gewicht);

                //double gewichtOpMaan = gewicht * 0.17;
                //Console.WriteLine($"Op de maan zou je {gewichtOpMaan}kg wegen");

                #endregion
                #region Oef 8
                //// Write an expression that checks for a given point {x, y} if it is within the circle K[{0, 0}, R=5].
                //// Explanation: the point {0, 0} is the center of the circle and 5 is the radius.
                ///
                //Console.Write("Wat is de straal van de cirkel: ");
                //int straal;
                //Int32.TryParse(Console.ReadLine(), out straal);

                //Console.Write("Wat is punt X: ");
                //int puntX;
                //Int32.TryParse(Console.ReadLine(), out puntX);

                //Console.Write("Wat is punt Y: ");
                //int puntY;
                //Int32.TryParse(Console.ReadLine(), out puntY);

                //if ((puntX * puntX)  + (puntY * puntY) <= (straal * straal)) //Stelling van pythagoras gebruiken
                //{
                //    Console.WriteLine($"Het punt {puntX},{puntY} ligt in de cirkel");
                //}
                //else
                //{
                //    Console.WriteLine($"Het punt {puntX},{puntY} ligt niet in de cirkel");
                //}

                #endregion
                #region Oef 9
                ////// Write an expression that checks for given point {x, y} if it is within the circle K[{0, 0}, R=5] and out of the rectangle [{-1, 1}, {5, 5}].
                /////  Clarification: for the rectangle the lower left and the upper right corners are given.
                /////
                //Console.Write("Wat is de straal van de cirkel: ");
                //int straal;
                //Int32.TryParse(Console.ReadLine(), out straal);

                //Console.Write("Op welk X punt ligt de linker onderhoek van de rechthoek: ");
                //int linkseOnderPunt;
                //Int32.TryParse(Console.ReadLine(), out linkseOnderPunt);

                //Console.Write("Op welk X punt ligt de rechter onderhoek van de rechthoek: ");
                //int rechterOnderPunt;
                //Int32.TryParse(Console.ReadLine(), out rechterOnderPunt);

                //Console.Write("Op welk Y punt liggen de bovenhoeken: ");
                //int bovenhoekenPunt;
                //Int32.TryParse(Console.ReadLine(), out bovenhoekenPunt);

                //Console.Write("Op welk Y punt liggen de onderhoeken: ");
                //int onderhoekenPunt;
                //Int32.TryParse(Console.ReadLine(), out onderhoekenPunt);


                //Console.Write("Wat is punt X: ");
                //int puntX;
                //Int32.TryParse(Console.ReadLine(), out puntX);

                //Console.Write("Wat is punt Y: ");
                //int puntY;
                //Int32.TryParse(Console.ReadLine(), out puntY);

                //bool puntInCirkel = ((puntX * puntX) + (puntY * puntY) <= (straal * straal)); //Stelling van pythagoras gebruiken
                //bool puntInRechthoek = ((puntX >= linkseOnderPunt) && (puntX <= rechterOnderPunt) && (puntY < bovenhoekenPunt) && (puntY >= onderhoekenPunt));

                //if  (puntInCirkel && puntInRechthoek)
                //{
                //    Console.WriteLine($"Het punt {puntX},{puntY} ligt in de cirkel en de rechthoek");
                //}
                //else
                //{
                //    Console.WriteLine($"Het punt {puntX},{puntY} ligt niet in beide");
                //}
                #endregion
                #region Oef 10
                ////// Write a program that takes as input a four-digit number in format abcd (e.g. 2011) and performs the following actions:
                //////  -Calculates the sum of the digits(in our example 2 + 0 + 1 + 1 = 4).      
                //////  -Prints on the console the number in reversed order: dcba(in our example 1102).
                //////  -Puts the last digit in the first position: dabc(in our example 1201).
                //////  -Exchanges the second and the third digits: acbd(in our example 2101).
                /////

                //Console.WriteLine("Geef een 4 nummer getal op: ");
                //int getal;
                //Int32.TryParse(Console.ReadLine(), out getal);

                ////Counting from the left
                //int digit4 = (getal % 10);
                //int digit3 = ((getal / 10) % 10);
                //int digit2 = ((getal / 100) % 10);
                //int digit1 = ((getal / 1000) % 10);

                //Console.WriteLine($"The digits for the number are {digit1} - {digit2} - {digit3} - {digit4}");
                //int sum = digit1 + digit2 + digit3 + digit4;
                //Console.WriteLine($"The sum of these digits is: {sum}");
                //Console.WriteLine($"The digits in reverse order: {digit4} - {digit3} - {digit2} - {digit1}");
                //Console.WriteLine($"Last digit in the first position: {digit4} - {digit1} - {digit2} - {digit3}");
                //Console.WriteLine($"Exchanges the second and the third digits {digit1} - {digit3} - {digit2} - {digit4}");

                #endregion
                #region Oef 14
                //Write a program that checks if a given number n(1 < n < 100) is a prime number(i.e.it is divisible without remainder only to itself and 1).

                //Volgens mijn eigen logica
                Console.WriteLine("Welk getal wil je controleren?");
                int getal;
                Int32.TryParse(Console.ReadLine(), out getal);

                int aantalDelers = 0;
                for (int i = 1; i <= getal; i++)
                {
                    if (getal % i == 0)
                    {
                        Console.WriteLine($"{i} is een deler van {getal}");
                        aantalDelers++;

                        if (aantalDelers > 2)
                            break;
                    }
                }

                Console.WriteLine((aantalDelers == 2 ? $"Getal {getal} is een priemgetal" : $"Getal {getal} is geen priemgetal"));

                #endregion

                Console.ReadKey();
        }
    }

    class Program_Koen
    {
        static void Expressions()
        {
            int number = 0;

            //1. Write an expression that checks whether an integer is odd or even.
            Console.Write("Check of nummer even is. Type een nummer: ");
            Int32.TryParse(Console.ReadLine(), out number);
            Console.WriteLine($"{number}" + ((number % 2 == 0) ? $" is even." : " is oneven"));

            Console.WriteLine("----------");

            //2. Write a Boolean expression that checks whether a given integer is
            //   divisible by both 5 and 7, without a remainder.
            Console.Write("Check of nummer deelbaar is door 5 en 7. Type een nummer: ");
            Int32.TryParse(Console.ReadLine(), out number);
            if ((number % 7 == 0) && (number % 5 == 0))
                Console.WriteLine($"{number} is zowel deelbaar door 5 en 7.");
            else
                Console.WriteLine($"{number} is niet deelbaar door 5 en/of 7.");

            Console.WriteLine("----------");

            //3. Write an expression that looks for a given integer if its third digit (right to left) is 7.
            Console.Write("Check of het 3de laatste nummer van een string 7 is. Type een nummer: ");
            Int32.TryParse(Console.ReadLine(), out number);

            string numberStr = number.ToString();
            //Check if number has 3 digits
            if ((numberStr.Length - 3) >= 0)
            {
                //Subdivide string and check third number
                string subdivStr = numberStr.Substring((numberStr.Length - 3), 1);
                int thirdLastNumber = 0;
                Int32.TryParse(subdivStr, out thirdLastNumber);

                Console.WriteLine($"{numberStr} zijn 3de laatste nummer is" + ((thirdLastNumber == 7) ? " wel" : " niet") + " 7.");
            }
            else
                Console.WriteLine($"{numberStr} is niet groot genoeg.");

            Console.WriteLine("----------");

            //6. Write a program that prints on the console the perimeter and the area
            //   of a rectangle by given side and height entered by the user.
            Console.WriteLine("Geef lengte en breedte in voor de oppervlakte en de lengte van de zijdes van een rechthoek");
            Console.Write("Lengte rechthoek. Type een nummer: ");
            int lengteRechthoek = 0;
            Int32.TryParse(Console.ReadLine(), out lengteRechthoek);
            Console.Write("Breedte rechthoek. Type een nummer: ");
            int breedteRechthoek = 0;
            Int32.TryParse(Console.ReadLine(), out breedteRechthoek);

            if (breedteRechthoek != 0 && lengteRechthoek != 0)
            {
                Console.WriteLine("Lengte van alle zijdes: " + (2 * (breedteRechthoek + lengteRechthoek)));
                Console.WriteLine("Oppervlakte van de rechthoek: " + (breedteRechthoek * lengteRechthoek));
            }
            else
                Console.WriteLine("Geen geldige lengte of breedte!");

            Console.WriteLine("----------");

            //7. The gravitational field of the Moon is approximately 17% of that on the
            //   Earth.Write a program that calculates the weight of a man on the
            //   moon by a given weight on the Earth.
            Console.Write("Hoeveel weeg jij op de maan? geef je gewicht in kg: ");
            Int32.TryParse(Console.ReadLine(), out number);
            float gewicht = number * 0.17f;
            Console.WriteLine($"Je weegt ongeveer {gewicht:0.00} kg op de maan!");

            Console.WriteLine("----------");

            // 8. Write an expression that checks for a given point {x, y} if it is within
            // the circle K({ 0, 0}, R = 5). Explanation: the point { 0, 0} is the center of
            // the circle and 5 is the radius.
            float circleX = 0;
            float circleY = 0;
            float circleRadius = 5;

            float userX = 0;
            float userY = 0;
            Console.WriteLine($"We kijken of een punt binnen een cirkel valt C{{{circleX},{circleY}}} met straal {circleRadius}");
            Console.Write("Jou x: ");
            float.TryParse(Console.ReadLine(), out userX);
            Console.Write("Jou y: ");
            float.TryParse(Console.ReadLine(), out userY);

            //calculate distance
            float dist = 0; // MathF.Sqrt(((userX - circleX) * (userX - circleX)) + ((userY - circleY) * (userY - circleY))); //Sqrt is quit heavy, if you don't have to use it don't

            Console.WriteLine($"Het punt {{{userX},{userY}}} ligt " + ((dist <= circleRadius) ? "wel" : "niet") + $" in de cirkel, zijn afstand tot het middenpunt is {dist:0.00}.");

            Console.WriteLine("----------");

            //9. Write an expression that checks for given point {x, y} if it is within the
            //   circle K({ 0, 0}, R = 5) and out of the rectangle[{ -1, 1}, { 5, 5}].
            //   Clarification: for the rectangle the lower left and the upper right corners are given.

            circleX = 0;
            circleY = 0;
            circleRadius = 5;

            float squareX1 = 1;
            float squareY1 = -1;
            float squareX2 = 5;
            float squareY2 = 5;

            userX = 0;
            userY = 0;
            Console.WriteLine($"We kijken of een punt binnen een cirkel valt C{{{circleX},{circleY}}} met straal {circleRadius} en niet in de rechthoek [{{{squareX1},{squareY1}}}{{{squareX2},{squareY2}}}]");
            Console.Write("Jou x: ");
            float.TryParse(Console.ReadLine(), out userX);
            Console.Write("Jou y: ");
            float.TryParse(Console.ReadLine(), out userY);

            //calculate distance
            dist = 0; // MathF.Sqrt(((userX - circleX) * (userX - circleX)) + ((userY - circleY) * (userY - circleY))); //Sqrt is quit heavy, if you don't have to use it don't

            bool isInSquare = (userX > squareX1 && userX < squareX2 && userY > squareY1 && userY < squareY2);
            Console.WriteLine($"Het punt {{{userX},{userY}}} ligt " + ((dist <= circleRadius && !isInSquare) ? "wel in de cirkel maar niet op de rechthoek." : "niet in de cirkel en/of op de rechthoek."));


            Console.WriteLine("----------");

            /*10. Write a program that takes as input a four-digit number in format abcd
             *    (e.g. 2011) and performs the following actions:
             *    - Calculates the sum of the digits (in our example 2+0+1+1 = 4).
             *    - Prints on the console the number in reversed order: dcba (in our
             *      example 1102).
             *    - Puts the last digit in the first position: dabc (in our example 1201).
             *    - Exchanges the second and the third digits: acbd (in our example 2101).*/
            Console.WriteLine("Geef een 4 cijferig nummer in (abcd). Type een nummer: ");
            Int32.TryParse(Console.ReadLine(), out number);

            numberStr = number.ToString();
            if (numberStr.Length == 4)
            {
                int cijferA = 0;
                int cijferB = 0;
                int cijferC = 0;
                int cijferD = 0;

                Int32.TryParse(numberStr.Substring(0, 1), out cijferA); //Kan ook via modulo berekend worden ipv Substrings!
                Int32.TryParse(numberStr.Substring(1, 1), out cijferB); //Dit is sneller en dus beter!
                Int32.TryParse(numberStr.Substring(2, 1), out cijferC);
                Int32.TryParse(numberStr.Substring(3, 1), out cijferD);

                int som = cijferA + cijferB + cijferC + cijferD;

                Console.WriteLine("De som van de individuele getallen is gelijk aan: {0,6}", som);
                Console.WriteLine("Het cijfer omgekeerd is: {0,6}", (cijferD + cijferC + cijferB + cijferA));
                Console.WriteLine("Het eerste en laatse getal geswitched: {0,6}", (cijferD + cijferB + cijferC + cijferA));
                Console.WriteLine("Het tweede en derde getal geswitched: {0,6}", (cijferA + cijferC + cijferB + cijferD));
            }
            else
                Console.WriteLine("Dit is geen 4 cijferig nummer!");

            Console.WriteLine("----------");

            //14. Write a program that checks if a given number n (1 < n < 100) is a
            //    prime number(i.e.it is divisible without remainder only to itself and 1).
            Console.WriteLine("Geef een cijfer tussen 1 en 100, we checken of het een priem getal is. Type een nummer: ");
            Int32.TryParse(Console.ReadLine(), out number);

            bool isPriemGetal = true;
            if (number > 1 && number < 100)
            {
                for (int i = 2; i < number; i++)
                {
                    if (number % i == 0)
                    {
                        isPriemGetal = false;
                        break;
                    }
                }

                Console.WriteLine(number + (isPriemGetal ? " is een priemgetal" : " is geen priemgetal"));
            }
            else
                Console.WriteLine("getal ligt niet tussen 1 en 100.");
        }

    }

    class Program_Niels
    {




        //arithmetic -, +, *, /, %, ++, -- 
        //logical &&, ||, !, ^ 
        //binary &, |, ^, ~, <<, >> 
        //comparison ==,!=, >, <, >=, <= 
        //assignment =, +=, -=, *=, /=, %=, &=, |=, ^=, <<=, >>= 
        //string concatenation + 
        //type conversion(type), as, is, typeof, sizeof 
        //other., new, (), [], ?:, ??




        static void Main(string[] args)
        {
            //beetje proberen met operators
            int x = 6;
            int y = 4;
            Console.WriteLine(x + y);
            Console.WriteLine(x - y);
            Console.WriteLine(x * y);
            Console.WriteLine(x / y);
            Console.WriteLine("modulus:");
            Console.WriteLine(x % y); //    6 / 4 = 1 modulus 2
            Console.WriteLine("y=" + (y = x++));
            Console.WriteLine("x=" + x);
            Console.WriteLine("y=" + (y = ++x));
            Console.WriteLine("x=" + x);
            Console.WriteLine(y *= 2);  // 8 
            int z = y = 3;              // y=3 and z=3 

            x = 6;
            y = 4;


            Console.WriteLine(z);       // 3 
            Console.WriteLine("x= " + x);
            //Console.WriteLine(x |= 2);  // 7 // dit is bit rekenen
            //Console.WriteLine("|=:");
            //Console.WriteLine(x = x | 10);
            Console.WriteLine(x += 4);  // 10 x = x+4
            Console.WriteLine(x /= 2);  // 5 x = x/2
            Console.WriteLine("x=" + x);
            Console.WriteLine("y=" + y);


            Console.WriteLine((x = 5) ^ (y = 5));


            // oefening 1
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 1:");
            Console.ForegroundColor = ConsoleColor.White;

            int numberToCheck = 20;

            if (numberToCheck % 2 == 0)
            {
                Console.WriteLine(numberToCheck + " is an even number");
            }
            else
            {
                Console.WriteLine(numberToCheck + " is an odd number");
            }


            // oefening 2

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 2:");
            Console.ForegroundColor = ConsoleColor.White;

            int numberToCheckTwo = 34;
            int divByFive = 5;
            int divBySeven = 7;

            if ((numberToCheckTwo % divByFive == 0) && (numberToCheckTwo % divBySeven == 0))
            {
                Console.WriteLine("deelbaar door 5 en 7 zonder restwaarde");
            }
            else
            {
                Console.WriteLine("NIET deelbaar door 5 en 7 zonder restwaarde");
            }


            //oefening 3 third digit = 7?

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 3:");
            Console.ForegroundColor = ConsoleColor.White;

            int thirdSeven = 4478; //checkt 3de cijfer van links
            int thirdDigit = (thirdSeven / 10) % 10;
            if (thirdDigit == 7)
            {
                Console.WriteLine("Het 3de cijfer is een " + thirdDigit);
            }
            else
            {
                Console.WriteLine("Het 3de cijfer is GEEN 7.");
            }


            //oefening 6: rechthoek

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 6 - rechthoek:");
            Console.ForegroundColor = ConsoleColor.White;

            int rechthoeksZijde1 = 10;
            int rechthoeksZijde2 = 15;

            int omtrekRechthoek = rechthoeksZijde1 * 2 + rechthoeksZijde2 * 2;
            int oppervlakteRechthoek = rechthoeksZijde1 * rechthoeksZijde2;


            Console.WriteLine("Omtrek Rechthoek = " + omtrekRechthoek + " cm");
            Console.WriteLine("Oppervlakte Rechthoek = " + oppervlakteRechthoek + " cm²");



            //oefening 6: extra: driehoek

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Oefening 6 - driehoek:");
            Console.ForegroundColor = ConsoleColor.White;

            int driehoekZijde1 = 10; // basis? in cm
            int driehoekZijde2 = 7;
            int driehoekZijde3 = 6;
            double driehoekHoogte = 4.13;

            Console.WriteLine(driehoekHoogte);

            int h = Convert.ToInt32(driehoekHoogte); //maakt er 4 van, dus niet nauwkeurig?

            Console.WriteLine(h);


            int omtrekDriehoek = driehoekZijde1 + driehoekZijde2 + driehoekZijde3;
            int oppervlakteDriehoek = driehoekZijde1 * h / 2;


            Console.WriteLine("Omtrek Driehoek = " + omtrekDriehoek + " cm");
            Console.WriteLine("Oppervlakte Driehoek = " + oppervlakteDriehoek + " cm²");


            //oefening 7: man on the moon

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 7:");
            Console.ForegroundColor = ConsoleColor.White;

            float gravitationalFieldMoon = 0.17f;
            int weightOnEarth = 80;

            Console.WriteLine("Weight on moon = " + weightOnEarth + " kg");
            Console.WriteLine("Weight on moon = " + weightOnEarth * gravitationalFieldMoon + " kg");



            //oefening 8+9: point in circle?

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 8+9:");
            Console.ForegroundColor = ConsoleColor.White;

            int xcoordinate = -6;
            int ycoordinate = 3;
            int radius = 5;


            if (xcoordinate * xcoordinate + ycoordinate * ycoordinate <= radius * radius)
            {
                // Console.WriteLine("Dit punt ligt in de Cirkel.");
                if ((xcoordinate < -1) || (ycoordinate < 1) || (xcoordinate > 5) || (ycoordinate > 5))


                {
                    Console.WriteLine("Het gegeven punt ligt in de cirkel en buiten de rechthoek!");
                }
                else
                {
                    Console.WriteLine("Het gegeven punt ligt in d cirkel, maar OOK in de rechthoek.");
                }
            }
            else
            {
                Console.WriteLine("Dit punt ligt NIET in de Cirkel.");
            }

            // oefening 10: abcd

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Begin oefening 10:");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Enter a four digit number:");
            int fourDigitNumber = Convert.ToInt32(Console.ReadLine());


            int d = fourDigitNumber % 10;
            Console.WriteLine("Last digit: " + d);
            int c = (fourDigitNumber / 10) % 10;
            Console.WriteLine("2nd digit: " + c);
            int b = (fourDigitNumber / 100) % 10;
            Console.WriteLine("3rd digit: " + b);
            int a = (fourDigitNumber / 1000) % 10;
            Console.WriteLine("4th digit: " + a);

            Console.WriteLine("Original Number: " + a + b + c + d);
            Console.WriteLine("Sum of digits: " + (a + b + c + d));
            Console.WriteLine("Reversed order: " + d + c + b + a);
            Console.WriteLine("Last digit in first place: " + d + a + b + c);
            Console.WriteLine("Exchange 2nd and 3rd: " + a + c + b + d);


            // Oefening 14: Write a program that checks if a given number n (1 < n < 100) is a prime number (i.e. it is divisible without remainder only to itself and 1).
            //nog niet klaar


        }
    }

    class Program_Joren
    {
        // Ex. 1
        static bool isEven(int val)
        {
            // Rest after division by 2 is 0 -> even
            return val % 2 == 0;
        }

        // Ex. 2
        // [divisors] - List of integers the value has to divide by with rest 0.
        static bool divisibleBy(int val, int[] divisors)
        {
            // Check each divisor for rest 0
            foreach (int div in divisors)
            {
                if (val % div != 0)
                    return false;
            }
            return true;
        }

        // Ex. 3
        static bool isNthDigitX(int val, int n, int x)
        {
            // Put n'th digit all the way right
            for (int i = 0; i < n; i++)
            {
                if (val < 10) // check to make sure n < # digits
                    return false;
                val /= 10;
            }
            // % 10 -> Drop everything except units
            return (val % 10) == x;
        }

        // Ex. 6
        static void rectangleInfo(double w, double h)
        {
            // Make sure our dimensions are valid
            if (w <= 0 || h <= 0)
                Console.WriteLine("Invalid width and/or height.");

            // Peri: 2b + 2h = 2 (b + h)
            // Area: b . h
            else
                Console.Write($"Rectangle {w}x{h}:\n\tPerimeter: {(w + h) * 2}\n\tArea: {w * h}\n");
        }

        // Ex. 7
        static double calculateMoonWeight(double earthWeight)
        {
            // moonWeight = earthWeight * 17%
            return (earthWeight / 100) * 17;
        }

        // Ex. 8
        // [pX, pY] - Point coordinate
        // [centerX, centerY] - Midpoint circle
        static bool isPointWithinCircle(double pX, double pY, double centerX, double centerY, double radius)
        {
            // Normalize to put center at origin incase center isn't at origin to make our lives easier
            pX -= centerX;
            pY -= centerY;

            // Calc distance from p to center : sqrt(pX^2 + pY^2)
            double d = Math.Sqrt(Math.Pow(pX, 2) + Math.Pow(pY, 2));
            return d <= radius;
        }

        // Ex. 9
        // [pX, pY] - Point coordinate
        // [rectX_LL, rectY_LL] - Point rectangle lower left
        // [rectX_UR, rectY_UR] - Point rectangle upper right
        static bool isPointWithinRectangle(double pX, double pY, double rectX_LL, double rectY_LL, double rectX_UR, double rectY_UR)
        {
            // Check if x is in range
            if (pX >= rectX_LL && pX <= rectX_UR)
            {
                // Check if y is in range
                if (pY >= rectY_LL && pY <= rectY_UR)
                {
                    return true;
                }
            }
            return false;
        }

        // Ex. 10
        static void FourDigitOperation()
        {
            // input
            int number = -1;
            while (number == -1)
            {
                // Make sure our input is a valid integer of length 4.
                Console.Clear();
                Console.Write("Input 4-digit number: ");
                if (Int32.TryParse(Console.ReadLine(), out number))
                {
                    if (number < 1000 || number > 9999)
                        number = -1;
                }
                else number = -1;
            }
            int buffer = number;

            // Calc sum:
            int SUMresult = 0;
            for (int i = 0; i < 4; i++)
            {
                SUMresult += (number % 10);
                number /= 10;
            }
            number = buffer;

            // Reverse order:
            int REVresult = 0;
            while (number > 0)
            {
                REVresult = REVresult * 10 + number % 10;
                number /= 10;
            }
            number = buffer;

            // Last digit in front
            int DIFresult = (number / 10) + ((number % 10) * 1000);
            number = buffer;

            // switch 2nd and 3rd digit
            int[] digits = new int[4];
            for (int i = 0; i < 4; i++)
            {
                digits[i] = number % 10;
                number /= 10;
            }
            int SWIresult = digits[3] * 1000 + digits[1] * 100 + digits[2] * 10 + digits[0];
            number = buffer;

            // print
            Console.Write($"Number: {number}" +
                $"\n\tSum of digits: {SUMresult}" +
                $"\n\tReverse order: {REVresult}" +
                $"\n\tLast digit infront: {DIFresult}" +
                $"\n\tDigit switch: {SWIresult}\n");
        }

        // Ex. 14
        static bool isPrime(int n)
        {
            // Range set by exercise
            if (1 < n && n < 100)
            {
                // Iterate over all integers from 1 to n^2
                for (int i = 2; i < Math.Pow(n, 2); i++)
                {
                    if (i != n && n % i == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    class Program_Hilde
    {
        static void run(string[] args)
        {
            // oefeningen p 160 - operatoren en expressies

            // QUESTION 1
            // Write an expression that checks whether an integer is odd or even.
            Console.WriteLine("QUESTION 1");
            int oddOrEven = 5;
            string testOddOrEven = oddOrEven % 2 == 0 ? "even" : "odd";
            Console.WriteLine("this number is " + testOddOrEven);




            // QUESTION 2
            Console.WriteLine("\nQUESTION 2");
            int intqtwo = 30;
            string testQ2 = (intqtwo % 5 == 0 && intqtwo % 7 == 0 ? "is" : "is NOT");
            Console.WriteLine("{0} {1} divisible by both 5 and 7 without remainder", intqtwo, testQ2);




            // QUESTION 3
            // Write an expression that looks for a given integer if its third digit (right to left) is 7.
            Console.WriteLine("\nQUESTION 3");
            int intQ3 = 2198795;
            int divideby100 = intQ3 / 100;
            Console.WriteLine(divideby100);// 987
            int resultQ3 = divideby100 % 10; // 7
            Console.Write("The third digit from the right of " + intQ3 + " is 7: ", intQ3);
            Console.WriteLine(resultQ3 == 7); // True/false




            // QUESTION 3-4: about bits




            // QUESTION 5 - kan ik nog eens proberen




            // QUESTION 6
            /* Write a program that prints on the console the perimeter and the area of a rectangle by 
             * given side and height entered by the user.*/
            Console.WriteLine("\nQUESTION 6");
            int side = 13;
            int height = 5;
            int perimeter = 2 * side + 2 * height;
            int area = side * height;
            Console.WriteLine("for a rectangle with measurements {0}cm x {1}cm: ", side, height);
            Console.WriteLine("perimeter is " + perimeter + "cm");
            Console.WriteLine("area is " + area + "cm2");




            // QUESTION 7
            /* The gravitational field of the Moon is approximately 17% of that on the Earth. 
             * Write a program that calculates the weight of a man on the moon by a given weight on the Earth.*/
            Console.WriteLine("\nQUESTION 7");
            float weightOnEearth = 60f;
            float weightOnMoon = 0.17f * weightOnEearth;
            Console.WriteLine(weightOnEearth + "kg on earth is " + weightOnMoon + "kg on the moon");




            // QUESTION 8
            Console.WriteLine("\nQUESTION 8");
            /* Write an expression that checks for a given point { x, y}
            if it is within the circle K({ 0, 0}, R = 5). 
            Explanation: the point { 0, 0} is the center of the circle and 5 is the radius.*/
            // Use the Pythagorean Theorem a2 + b2 = c2.The point is inside the circle when(x* x) +(y * y) ≤ 5 * 5.
            float radius = 5f;
            float pointx = 0f;
            float pointy = -5f;
            float testQ8 = pointx * pointx + pointy * pointy;
            bool testCircle = (testQ8 <= radius * radius);
            Console.WriteLine(testCircle ? "The point is inside circle" : "The point is outside circle");




            // QUESTION 9
            /* Write an expression that checks for given point {x, y} if it is within the circle
             * K({0, 0}, R=5) and out of the rectangle [{-1, 1}, {5, 5}]. Clarification: for the 
             * rectangle the lower left and the upper right corners are given. */
            Console.WriteLine("\n QUESTION 9");

            bool testRectangle = (-1 <= pointx && pointx <= 5) && (1 <= pointy && pointy <= 5);
            Console.WriteLine(testCircle == true && testRectangle == true ? "The point is inside area" : "The point is outside area");




            // QUESTION 10
            /* Write a program that takes as input a four-digit number in format abcd (e.g. 2011) 
             * and performs the following actions:
             * - Calculates the sum of the digits (in our example 2+0+1+1 = 4).
             * - Prints on the console the number in reversed order: dcba (in our example 1102).
             * - Puts the last digit in the first position: dabc (in our example 1201).
             * - Exchanges the second and the third digits: acbd (in our example 2101). */
            Console.WriteLine("\nQUESTION 10");
            Console.WriteLine("enter a four-digit number in format abcd (eg 2011)");
            int numQ10 = Convert.ToInt32(Console.ReadLine());
            while (numQ10.ToString().Length != 4)
            {
                Console.WriteLine("input must be four-digit number");
                Console.WriteLine("enter a four-digit number in format abcd (eg 2011)");
                numQ10 = Convert.ToInt32(Console.ReadLine());
            }
            int num1 = (numQ10 - (numQ10 % 1000)) / 1000;
            int num2 = (numQ10 % 1000 - numQ10 % 100) / 100;
            int num3 = (numQ10 % 100 - numQ10 % 10) / 10;
            int num4 = numQ10 % 10;
            int sum = num1 + num2 + num3 + num4;
            Console.WriteLine("the sum is " + sum);
            Console.WriteLine("in reverse: " + num4 + num3 + num2 + num1);
            Console.WriteLine("exchange 2nd and 3rd digit: " + num1 + num3 + num2 + num4);



            //Q11-13 andere keer bekijken



            //QUESTION 14
            /*Write a program that checks if a given number n (1 < n < 100) is a prime number
             * (i.e. it is divisible without remainder only to itself and 1).*/
            Console.WriteLine("\nQUESTION 14");
            Console.WriteLine("enter number between 1 and 100 (1 < n < 100)");
            int primeNr = Convert.ToInt32(Console.ReadLine());
            while (primeNr <= 1 || primeNr >= 100)
            {
                Console.WriteLine("enter number between 1 and 100 (1 < n < 100)");
                primeNr = Convert.ToInt32(Console.ReadLine());
            }

            int aantal = 0;
            for (int i = 1; i <= primeNr; i++)
            {
                if (primeNr % i == 0)
                {
                    aantal++;
                }
            }
            Console.WriteLine(primeNr + " is " + (aantal == 2 ? "een " : "geen ") + " priemgetal");



            //Q15-16 andere keer bekijken
        }
    }


    class Program_Scott
    {
        static void run(string[] args)
        {

            // oef 1

            int evenOneven;
            Console.Write("Geef getal in : ");
            evenOneven = Convert.ToInt32(Console.ReadLine());


            if (evenOneven % 2 == 0) 
            {
                Console.WriteLine("Het getal is even");
            }
            {
                Console.WriteLine("Het getal is oneven");
            }

            // oef 2

            int getal = 35;
            bool restDoor7 = true;
            bool restDoor5 = true;

            if (getal % 5 == 0) ;
            {
                restDoor5 = true;
            }
            if (getal % 7 == 0) ;
            {
                restDoor7 = true;
            }

            Console.WriteLine(restDoor5 && restDoor7);


            // oef 3

            bool derdeCijfer7 = true;
            int getalMet7 = 1700;
            int getal2 = getalMet7 / 100;
            if (getal2 % 10 == 7) 
            {
                derdeCijfer7 = true;
            }

            Console.WriteLine(derdeCijfer7);

            // oef 6

            Console.Write("Geef lengte : ");
            int lengte = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef breedte : ");
            int breedte = Convert.ToInt32(Console.ReadLine());

            int omtrek = (lengte * 2) + (breedte * 2);
            int oppervlak = (lengte * breedte);

            Console.WriteLine("De omtrek is " + omtrek);
            Console.WriteLine("De oppervlakte is " + oppervlak);

            // oef 7

            Console.Write("Geef gewicht : ");
            int gewichtAarde = Convert.ToInt32(Console.ReadLine());

            int gewichtMaan = gewichtAarde / 100 * 17;

            Console.WriteLine("Het gewicht op de maan is " + gewichtMaan);

            // oef 8

            // oef 9

            // oef 10

            Console.Write("Geef digit 1 : ");
            int digit1 = Convert.ToInt32(Console.ReadKey());
            Console.Write("Geef digit 2 : ");
            int digit2 = Convert.ToInt32(Console.ReadKey());
            Console.Write("Geef digit 3 : ");
            int digit3 = Convert.ToInt32(Console.ReadKey());
            Console.Write("Geef digit 4 : ");
            int digit4 = Convert.ToInt32(Console.ReadKey());
            int som = digit1 + digit2 + digit3 + digit4;


            Console.WriteLine(som);
            Console.WriteLine(digit4 + digit3 + digit2 + digit1);
            Console.WriteLine(digit4 + digit1 + digit2 + digit3);
            Console.WriteLine(digit1 + digit3 + digit2 + digit4);


            // oef 14

            //Loop voor restwaarde bij deling door 2 tot n-1, als de restwaarde 0 geeft is het geen priemgetal


            int priemGetal;
            Console.Write("Geef getal tussen 1 en 100 : ");
            priemGetal = Convert.ToInt32(Console.ReadLine());

            for (int i = 2; i < priemGetal; i++) 
            {
                int restPriem = priemGetal % i;
                if (restPriem == 0) 
                {
                    Console.WriteLine("Dit is geen priemgetal");
                }
                {
                    Console.WriteLine("Dit is een priemgetal");
                }
            }
            //zou deze graag afmaken morgenavond of woensdag tijdens de zelfstudie als we morgen ook het hoofdstuk van de loops hebben doorlopen.

        }
    }




}
